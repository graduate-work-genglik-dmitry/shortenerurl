﻿namespace ShortenerURL.Exceptions
{
    public class InvalidEmailException : BaseApplicationException
    {
        public InvalidEmailException() : base("Invalid email address")
        {
        }
    }
}