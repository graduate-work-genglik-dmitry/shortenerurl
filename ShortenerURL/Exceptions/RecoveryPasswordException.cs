﻿namespace ShortenerURL.Exceptions
{
    public class RecoveryPasswordException : BaseApplicationException
    {
        public RecoveryPasswordException() : base(
            "Your account is not verified. It is impossible to recover the password.")
        {
        }
    }
}