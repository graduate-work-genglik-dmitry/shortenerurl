﻿namespace ShortenerURL.Exceptions
{
    public class NoAccessException : BaseApplicationException
    {
        public NoAccessException(string message) : base(message)
        {
        }
    }
}