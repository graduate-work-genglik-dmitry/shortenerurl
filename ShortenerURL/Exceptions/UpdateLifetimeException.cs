﻿namespace ShortenerURL.Exceptions
{
    public class UpdateLifetimeException : BaseApplicationException
    {
        public UpdateLifetimeException(string message) : base(message)
        {
        }
    }
}