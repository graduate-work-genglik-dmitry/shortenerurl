﻿namespace ShortenerURL.Exceptions
{
    public class DisposableUrlUpdateException : BaseApplicationException
    {
        public DisposableUrlUpdateException() : base("One-time link cannot be updated")
        {
        }
    }
}