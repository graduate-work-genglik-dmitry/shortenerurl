﻿namespace ShortenerURL.Exceptions
{
    public class InvalidUsernameException : BaseApplicationException
    {
        public InvalidUsernameException() : base(
            "Username does not meet the requirements: 4-20 characters, no spaces, no dots or underscores at the ends")
        {
        }
    }
}