﻿namespace ShortenerURL.Exceptions
{
    public class UrlException : BaseApplicationException
    {
        public UrlException(string message) : base(message)
        {
        }
    }
}