﻿namespace ShortenerURL.Exceptions
{
    public class AccountAlreadyExistsException : BaseApplicationException
    {
        public AccountAlreadyExistsException() : base("Account already exists")
        {
        }
    }
}