﻿namespace ShortenerURL.Exceptions
{
    public class EmptyUpdateException : BaseApplicationException
    {
        public EmptyUpdateException() : base("Nothing to update")
        {
        }
    }
}