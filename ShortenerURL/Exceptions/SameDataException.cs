﻿namespace ShortenerURL.Exceptions
{
    public class SameDataException: BaseApplicationException
    {
        public SameDataException(string message) : base(message)
        {
        }
    }
}