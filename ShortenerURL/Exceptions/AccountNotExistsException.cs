﻿namespace ShortenerURL.Exceptions
{
    public class AccountNotExistsException: BaseApplicationException
    {
        public AccountNotExistsException() : base("Account doesn't exists")
        {
        }
    }
}