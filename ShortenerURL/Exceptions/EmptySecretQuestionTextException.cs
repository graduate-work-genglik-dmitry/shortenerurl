﻿namespace ShortenerURL.Exceptions
{
    public class EmptySecretQuestionTextException : BaseApplicationException
    {
        public EmptySecretQuestionTextException() : base("Security question text is empty or not set")
        {
        }
    }
}