﻿namespace ShortenerURL.Exceptions
{
    public class NoSecurityException : BaseApplicationException
    {
        public NoSecurityException() : base("No password or security question")
        {
        }
    }
}