﻿namespace ShortenerURL.Exceptions
{
    public class UrlLifetimeException : BaseApplicationException
    {
        public UrlLifetimeException(string message) : base(message)
        {
        }
    }
}