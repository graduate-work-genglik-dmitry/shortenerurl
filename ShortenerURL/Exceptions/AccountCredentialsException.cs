﻿namespace ShortenerURL.Exceptions
{
    public class AccountCredentialsException : BaseApplicationException
    {
        public AccountCredentialsException() : base("Username or password not specified")
        {
        }

        public AccountCredentialsException(string message) : base(message)
        {
        }
    }
}