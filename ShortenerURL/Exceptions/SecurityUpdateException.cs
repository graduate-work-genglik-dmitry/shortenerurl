﻿namespace ShortenerURL.Exceptions
{
    public class SecurityUpdateException : BaseApplicationException
    {
        public SecurityUpdateException(string message) : base(message)
        {
        }
    }
}