﻿namespace ShortenerURL.Exceptions
{
    public class InvalidPasswordException : BaseApplicationException
    {
        public InvalidPasswordException() : base(
            "The password does not meet the requirements: 8-14 characters, at least one number, at least one uppercase character, no spaces")
        {
        }
    }
}