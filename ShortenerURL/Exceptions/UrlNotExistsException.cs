﻿namespace ShortenerURL.Exceptions
{
    public class UrlNotExistsException : BaseApplicationException
    {
        public UrlNotExistsException() : base("Url doesn't exists")
        {
        }
    }
}