﻿using System;

namespace ShortenerURL.Exceptions
{
    public class BaseApplicationException : Exception
    {
        protected BaseApplicationException(string message) : base(string.IsNullOrWhiteSpace(message)
            ? "Something went wrong"
            : message)
        {
        }
    }
}