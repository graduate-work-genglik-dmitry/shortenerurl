﻿namespace ShortenerURL.Domain.AdditionalModels
{
    public class Location
    {
        public string Status { get; set; }
        public string Country { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
    }
}