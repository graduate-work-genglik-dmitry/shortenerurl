﻿namespace ShortenerURL.Domain.AuthModels
{
    public class AccountProfileUpdate
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string CurrentPassword { get; set; }
    }
}