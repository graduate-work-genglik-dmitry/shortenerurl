﻿namespace ShortenerURL.Domain.AuthModels
{
    public class AccountProfileInfo : Entity
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Password2 { get; set; }
        public bool Confirmed { get; set; } = false;
        public bool AccessIsAllowed { get; set; } = false;
    }
}