﻿namespace ShortenerURL.Domain.AuthModels
{
    public class PasswordRecoveryModel
    {
        public string Email { get; set; }
    }
}