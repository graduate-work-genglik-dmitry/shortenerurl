﻿namespace ShortenerURL.Domain.AuthModels
{
    public class LoginModel
    {
        public string UsernameOrEmail { get; set; }
        public string Password { get; set; }
        public bool Email { get; set; } = false;
    }
}