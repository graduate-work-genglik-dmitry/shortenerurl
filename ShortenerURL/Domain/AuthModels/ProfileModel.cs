﻿using System;

namespace ShortenerURL.Domain.AuthModels
{
    public class ProfileModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool Confirmed { get; set; }
        public bool AccessIsAllowed { get; set; }
    }
}