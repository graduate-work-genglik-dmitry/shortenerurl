﻿namespace ShortenerURL.Domain.Enums
{
    public enum OrderMode
    {
        ModifiedOn = 0,
        CreatedOn = 1,
        Expiration = 2,
        RedirectCount = 3
    }
}