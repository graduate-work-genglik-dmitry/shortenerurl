﻿namespace ShortenerURL.Domain.Enums
{
    public enum AccessMode
    {
        Free = 0,
        SecretQuestion = 1,
        Password = 2,
        Disposable = 3
    }
}
