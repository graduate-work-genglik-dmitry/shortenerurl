﻿using System;

namespace ShortenerURL.Domain
{
    public class Audit
    {
        public DateTime CreatedOn { get; protected set; }
        public DateTime ModifiedOn { get; protected set; }

        public void Modify()
        {
            var now = DateTime.UtcNow;
            if (CreatedOn == default)
            {
                CreatedOn = now;
            }
            ModifiedOn = now;
        }
    }
}
