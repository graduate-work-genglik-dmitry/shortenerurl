﻿using System;

namespace ShortenerURL.Domain
{
    public interface IIdentity
    {
        void SetId(Guid id);
        void GenerateId();
    }
}
