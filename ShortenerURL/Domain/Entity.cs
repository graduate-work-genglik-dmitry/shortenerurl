﻿using System;

namespace ShortenerURL.Domain
{
    public abstract class Entity : IIdentity
    {
        public Guid Id { get; protected set; }
        public Audit Audit { get; protected set; }

        protected Entity()
        {
            Audit = new Audit();
        }

        public void GenerateId()
        {
            Id = Guid.NewGuid();
        }

        public void SetId(Guid id)
        {
            Id = id;
        }
    }
}
