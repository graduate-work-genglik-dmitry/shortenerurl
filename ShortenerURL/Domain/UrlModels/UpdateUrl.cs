﻿using System;

namespace ShortenerURL.Domain.UrlModels
{
    public class UpdateUrl
    {
        public Guid UrlId { get; set; }
        public long? AddedLifetime { get; set; }
        public long? ReducedLifetime { get; set; }
        public string Password { get; set; }
        public string SecretQuestion { get; set; }
        public string SecretQuestionText { get; set; }
        public bool? IsEternal { get; set; }
    }
}