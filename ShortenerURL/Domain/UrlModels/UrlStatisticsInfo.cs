﻿using System;

namespace ShortenerURL.Domain.UrlModels
{
    public class UrlStatisticsInfo
    {
        public string Ip { get; set; }
        public string Location { get; set; }
        public string Device { get; set; }
        public string OperationSystem { get; set; }
        public DateTime TimeOfVisit { get; set; }
    }
}
