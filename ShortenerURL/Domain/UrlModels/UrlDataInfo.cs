﻿using System;
using System.Collections.Generic;
using ShortenerURL.Domain.Enums;

namespace ShortenerURL.Domain.UrlModels
{
    public class UrlDataInfo : Entity
    {
        public Guid AccountId { get; set; }

        public string SourceUrl { get; set; }
        public string ShortUrlId { get; set; }
        public DateTime? Expiration { get; set; }
        public long? Lifetime { get; set; }
        public AccessMode Mode { get; set; }

        public string SecretQuestion { get; set; }
        public string SecretQuestionText { get; set; }
        public string Password { get; set; }
        public bool? IsDisposable { get; set; }
        public bool? IsEternal { get; set; }

        public IList<UrlStatisticsInfo> StatisticsInfo { get; set; } = new List<UrlStatisticsInfo>();
        public long Clicks { get; set; }
    }
}