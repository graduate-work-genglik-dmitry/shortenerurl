﻿using ShortenerURL.Domain.Enums;

namespace ShortenerURL.Domain.UrlModels
{
    public class NewUrl
    {
        public string SourceUrl { get; set; }
        public AccessMode Mode { get; set; }
        public long? Lifetime { get; set; }
        public string PasswordOrSecret { get; set; }
        public string SecretQuestionText { get; set; }
        public bool IsEternal { get; set; }
    }
}