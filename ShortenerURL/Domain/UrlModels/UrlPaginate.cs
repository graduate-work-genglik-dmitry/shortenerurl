﻿using System.Collections.Generic;

namespace ShortenerURL.Domain.UrlModels
{
    public class UrlPaginate
    {
        public long TotalLength { get; set; }
        public int PageIndex { get; set; }
        public int? GlobalStartIndex { get; set; }
        public IEnumerable<UrlDataInfo> PageItems { get; set; }
        public bool IsEmpty { get; set; }
    }
}