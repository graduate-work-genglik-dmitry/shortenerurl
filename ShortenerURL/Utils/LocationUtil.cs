﻿using System.Net.Http;
using System.Threading.Tasks;
using ShortenerURL.Domain.AdditionalModels;
using Newtonsoft.Json;

namespace ShortenerURL.Utils
{
    public static class LocationUtil
    {
        private static readonly HttpClient HttpClient = HttpClientFactoryUtil.CreateHttpClient();

        public static async Task<Location> GetLocation(string ip)
        {
            var requestUrl = $"http://ip-api.com/json/{ip}?fields=status,country,regionName,city";
            var response = await HttpClient.GetAsync(requestUrl);
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Location>(responseString);
        }
    }
}