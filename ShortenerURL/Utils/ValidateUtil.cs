﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using ShortenerURL.Domain.AuthModels;
using ShortenerURL.Domain.Enums;
using ShortenerURL.Domain.UrlModels;
using ShortenerURL.Exceptions;
using ShortenerURL.Utils.Cryptography;

namespace ShortenerURL.Utils
{
    public static class ValidateUtil
    {
        public static void UrlValidator(NewUrl newUrlData)
        {
            if (newUrlData.SourceUrl == null)
            {
                throw new UrlException("No url");
            }

            if (!Uri.IsWellFormedUriString(newUrlData.SourceUrl, UriKind.Absolute))
            {
                throw new UrlException("Invalid url");
            }

            switch (newUrlData.Lifetime)
            {
                case null:
                    throw new UrlLifetimeException("Link lifetime not set");
                case < 15:
                    throw new UrlLifetimeException("Life time less than 15 minutes");
                case > 44640:
                    throw new UrlLifetimeException("Life time more than 31 days");
            }

            if (newUrlData.Mode is AccessMode.Password or AccessMode.SecretQuestion &&
                string.IsNullOrWhiteSpace(newUrlData.PasswordOrSecret))
            {
                throw new NoSecurityException();
            }

            switch (newUrlData.Mode)
            {
                case AccessMode.SecretQuestion when string.IsNullOrWhiteSpace(newUrlData.SecretQuestionText):
                    throw new EmptySecretQuestionTextException();
                case AccessMode.Password when !PasswordValidator(newUrlData.PasswordOrSecret):
                    throw new InvalidPasswordException();
            }
        }

        public static void AccountProfileValidator(string password, string repeatPassword = null,
            string username = null, string email = null)
        {
            if (password == null || username == null && email == null)
            {
                throw new AccountCredentialsException();
            }

            if (username != null && !UsernameValidator(username))
            {
                throw new InvalidUsernameException();
            }

            if (email != null && !EmailValidator(email))
            {
                throw new InvalidEmailException();
            }

            if (!PasswordValidator(password))
            {
                throw new InvalidPasswordException();
            }

            if (repeatPassword != null && password != repeatPassword)
            {
                throw new AccountCredentialsException("Password mismatch");
            }
        }

        public static bool PasswordValidator(string password)
        {
            return Regex.IsMatch(password,
                @"^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])[\da-zA-Z(.,:;?!*+%-<>@[\]{}\/_{}$#)]{8,14}$");
        }

        private static bool EmailValidator(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
        }

        private static bool UsernameValidator(string username)
        {
            return Regex.IsMatch(username, @"^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$");
        }

        public static void UpdateUrlDataValidator(UpdateUrl urlUpdateData, UrlDataInfo url)
        {
            if (url.IsDisposable == true)
            {
                throw new DisposableUrlUpdateException();
            }

            if (urlUpdateData.Password != null && urlUpdateData.SecretQuestion != null)
            {
                throw new SecurityUpdateException("You cannot set a password and a security question at the same time");
            }

            if (urlUpdateData.Password != null && url.Password == null && url.SecretQuestion != null)
            {
                throw new SecurityUpdateException("You cannot set a password if you already have a secret question");
            }

            if (urlUpdateData.SecretQuestion != null && url.SecretQuestion == null && url.Password != null)
            {
                throw new SecurityUpdateException("You cannot set a secret question if you already have a password");
            }

            if (urlUpdateData.SecretQuestion != null && string.IsNullOrWhiteSpace(urlUpdateData.SecretQuestionText))
            {
                throw new EmptySecretQuestionTextException();
            }

            if (urlUpdateData.AddedLifetime != null && urlUpdateData.ReducedLifetime != null)
            {
                throw new UpdateLifetimeException("You can either add or decrease the time, but not at the same time");
            }

            if (urlUpdateData.AddedLifetime < 0 || urlUpdateData.ReducedLifetime < 0)
            {
                throw new UpdateLifetimeException("Time cannot be less than 0");
            }

            if (urlUpdateData.AddedLifetime == null && urlUpdateData.ReducedLifetime == null &&
                urlUpdateData.IsEternal == false)
            {
                throw new UpdateLifetimeException("Time not set");
            }

            if (urlUpdateData.Password == null && urlUpdateData.SecretQuestion == null &&
                urlUpdateData.IsEternal == null && urlUpdateData.ReducedLifetime == null &&
                urlUpdateData.AddedLifetime == null && urlUpdateData.SecretQuestionText == null)
            {
                throw new EmptyUpdateException();
            }
        }

        public static void UpdateAccountProfileValidator(AccountProfileInfo oldProfile,
            AccountProfileUpdate updateProfile)
        {
            var identicalUsernames = false;
            var identicalPasswords = false;

            if (!HashUtil.CheckMatch(updateProfile.CurrentPassword, oldProfile.Password))
            {
                throw new AccountCredentialsException("The current password was entered incorrectly");
            }

            if (updateProfile.Username == null && updateProfile.Email == null && updateProfile.NewPassword == null)
            {
                throw new EmptyUpdateException();
            }

            if (updateProfile.Username != null)
            {
                if (AesCryptUtil.Encrypt(updateProfile.Username, oldProfile.Password2) == oldProfile.Username)
                {
                    identicalUsernames = true;
                    throw new SameDataException("Identical usernames");
                }

                if (!UsernameValidator(updateProfile.Username))
                {
                    throw new InvalidUsernameException();
                }
            }

            if (updateProfile.NewPassword != null)
            {
                if (HashUtil.CheckMatch(updateProfile.NewPassword, oldProfile.Password))
                {
                    identicalPasswords = true;
                    throw new SameDataException("Identical passwords");
                }

                if (!PasswordValidator(updateProfile.NewPassword))
                {
                    throw new InvalidPasswordException();
                }
            }

            if (updateProfile.Email != null)
            {
                if (AesCryptUtil.Encrypt(updateProfile.Email) == oldProfile.Email)
                {
                    throw identicalUsernames switch
                    {
                        true when identicalPasswords => new SameDataException(
                            "Completely identical objects. Nothing to update"),
                        _ => new SameDataException("Identical email's")
                    };
                }

                if (!EmailValidator(updateProfile.Email))
                {
                    throw new InvalidEmailException();
                }
            }
        }
    }
}