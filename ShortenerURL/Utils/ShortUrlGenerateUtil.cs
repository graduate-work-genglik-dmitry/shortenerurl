﻿using System;
using ShortenerURL.Domain.UrlModels;

namespace ShortenerURL.Utils
{
    public static class ShortUrlGenerateUtil
    {
        private const string Alphabet = "abcdefghijklmnopqrstuvwxyz01234567890=";

        public static string GenerateShortUrlId => Generate();

        private static string Generate()
        {
            var shortUrlId = string.Empty;

            for (var i = 0; i < 9; i++) shortUrlId += Alphabet[GenerateDigit()];

            return shortUrlId;
        }

        private static int GenerateDigit()
        {
            var rng = new Random();
            return rng.Next(Alphabet.Length);
        }

        public static string Uniqueness(UrlDataInfo urlData, string notUniqueShortUrl)
        {
            if (urlData == null)
            {
                return string.Empty;
            }

            var newShortUrl = Generate();

            while (newShortUrl == notUniqueShortUrl)
            {
                newShortUrl = Generate();
            }

            return newShortUrl;
        }
    }
}