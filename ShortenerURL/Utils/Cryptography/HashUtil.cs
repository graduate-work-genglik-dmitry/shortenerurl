﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ShortenerURL.Utils.Cryptography
{
    public static class HashUtil
    {
        private static string GenerateSalt()
        {
            var saltSize = GetRandomInteger(8, 15);
            var salt = string.Empty;

            for (var i = 0; i <= saltSize; i++)
            {
                var ch = (char) GetRandomInteger(33, 126);
                if (char.IsLetterOrDigit(ch))
                {
                    salt += ch;
                    continue;
                }

                i--;
            }

            return salt.ToLower();
        }

        private static string GenerateHash(byte[] sequence)
        {
            var shaM = new SHA512Managed();
            var hash = shaM.ComputeHash(sequence);
            shaM.Dispose();

            var hashString = hash.Aggregate(string.Empty, (current, x) => current + $"{x:x2}");

            return hashString;
        }

        public static string GetHashString(string charSequence, bool withSalt = true)
        {
            if (charSequence == null)
            {
                return null;
            }

            if (withSalt)
            {
                var salt = GenerateSalt();
                var hashString = GenerateHash(Encoding.Unicode.GetBytes(salt + charSequence));

                return salt + hashString + salt.Length.ToString("X").ToLower();
            }

            return GenerateHash(Encoding.Unicode.GetBytes(charSequence));
        }

        public static bool CheckMatch(string charSequence, string hashString)
        {
            var saltSize = Convert.ToInt32(hashString[^1].ToString(), 16);
            var salt = hashString.Substring(0, saltSize);
            var hashStringForCheck = GenerateHash(Encoding.Unicode.GetBytes(salt + charSequence));

            hashString = hashString.Remove(hashString.Length - 1).Remove(0, saltSize);

            return hashStringForCheck == hashString;
        }

        private static int GetRandomInteger(int min, int max)
        {
            var rng = new Random();
            return rng.Next(min, max);
        }
    }
}