﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ShortenerURL.Utils.Cryptography
{
    public static class AesCryptUtil
    {
        // Static password for specific cases
        private const string DefaultPassword = "sP0F6$9mBewso|ZMHG$GMg7WTpUZ@JK4";

        public static string Encrypt(string plainText, string password = DefaultPassword)
        {
            if (string.IsNullOrEmpty(plainText))
            {
                return null;
            }

            var salt = GetSalt(password);
            var initialVector = GetInitialVector(salt);
            var derivedPassword = new Rfc2898DeriveBytes(password, salt, 5000, HashAlgorithmName.SHA256);

            var symmetricKey = new AesManaged
            {
                Key = derivedPassword.GetBytes(256 / 8),
                IV = initialVector
            };

            byte[] cipherTextBytes;

            using (var encryptor = symmetricKey.CreateEncryptor())
            {
                using (var memStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (var cryptoStreamWriter = new StreamWriter(cryptoStream))
                        {
                            cryptoStreamWriter.Write(plainText);
                        }

                        cipherTextBytes = memStream.ToArray();
                    }
                }
            }

            symmetricKey.Clear();

            return Convert.ToBase64String(cipherTextBytes);
        }
        
        public static string Decrypt(string cipherText, string password = DefaultPassword)
        {
            if (string.IsNullOrEmpty(cipherText))
            {
                return null;
            }

            var cipherTextBytes = Convert.FromBase64String(cipherText);
            var salt = GetSalt(password);
            var initialVector = GetInitialVector(salt);
            var derivedPassword = new Rfc2898DeriveBytes(password, salt, 5000, HashAlgorithmName.SHA256);

            var symmetricKey = new AesManaged
            {
                Key = derivedPassword.GetBytes(256 / 8),
                IV = initialVector
            };

            string decryptedText;

            using (var decryptor = symmetricKey.CreateDecryptor())
            {
                using (var memStream = new MemoryStream(cipherTextBytes))
                {
                    using (var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var cryptoStreamReader = new StreamReader(cryptoStream))
                        {
                            decryptedText = cryptoStreamReader.ReadToEnd();
                        }
                    }
                }
            }

            symmetricKey.Clear();

            return decryptedText;
        }

        private static byte[] GetSalt(string sequence)
        {
            var sha512Salt = HashUtil.GetHashString(sequence, false);
            var salt = string.Empty;

            for (var i = 1; i <= 16; i++)
            {
                salt += $"{sha512Salt[i]}{sha512Salt[^i]}";
            }

            return Encoding.ASCII.GetBytes(salt);
        }

        private static byte[] GetInitialVector(byte[] sequence)
        {
            var sha512Vector = HashUtil.GetHashString(Convert.ToBase64String(sequence), false);
            var vector = string.Empty;

            for (var i = 1; i <= 8; i++)
            {
                vector += $"{sha512Vector[i * 2]}{sha512Vector[^(i * 2)]}";
            }

            return Encoding.ASCII.GetBytes(vector);
        }
    }
}