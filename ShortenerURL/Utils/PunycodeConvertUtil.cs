﻿using System;
using System.Globalization;
using System.Linq;

namespace ShortenerURL.Utils
{
    public static class PunycodeConvertUtil
    {
        private static readonly IdnMapping Idn = new();

        public static string Encode(string url)
        {
            var cutArray = url.Replace("://", " ").Replace("/", " ")
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var protocol = cutArray[0] + "://";
            cutArray[0] = string.Empty;

            url = cutArray.Where(str => str != string.Empty)
                .Aggregate(protocol, (current, str) => current + (Idn.GetUnicode(str) + '/'));

            return url;
        }

        public static string Decode(string unicodeUrl)
        {
            var cutArray = unicodeUrl.Replace("://", " ").Replace("/", " ")
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);
            var protocol = cutArray[0] + "://";
            cutArray[0] = string.Empty;

            unicodeUrl = cutArray.Where(str => str != string.Empty)
                .Aggregate(protocol, (current, str) => current + (Idn.GetUnicode(str) + '/'));

            return unicodeUrl;
        }
    }
}