﻿using System.Net.Http;

namespace ShortenerURL.Utils
{
    public static class HttpClientFactoryUtil
    {
        private static readonly object Locker = new();
        private static HttpClient _httpClient;

        public static HttpClient CreateHttpClient()
        {
            if (_httpClient == null)
            {
                lock (Locker)
                {
                    _httpClient ??= new HttpClient();
                }
            }

            return _httpClient;
        }
    }
}