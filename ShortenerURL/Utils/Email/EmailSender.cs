﻿using System.IO;
using System.Threading.Tasks;
using FluentEmail.Core;
using FluentEmail.MailKitSmtp;

namespace ShortenerURL.Utils.Email
{
    public static class EmailSender
    {
        public const string Email = "genglik99@gmail.com";

        public static SmtpClientOptions ClientOptions { get; } = new()
        {
            Server = "smtp.gmail.com",
            Port = 465,
            User = "genglik99@gmail.com",
            Password = "ziceoxonnmsndkjt",
            UseSsl = true,
            RequiresAuthentication = true
        };

        public static async Task SendEmail(IFluentEmail emailService, EmailParameters parameters)
        {
            emailService.To(parameters.Recipient)
                .Subject(parameters.Subject)
                .UsingTemplateFromFile(
                    $"{Directory.GetCurrentDirectory()}/Pages/EmailTemplates/{parameters.TemplateName}.cshtml",
                    parameters.Model);

            var response = await emailService.SendAsync();

            emailService.Data.ToAddresses.Clear();
        }
    }
}