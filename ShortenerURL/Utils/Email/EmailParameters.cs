﻿namespace ShortenerURL.Utils.Email
{
    public class EmailParameters
    {
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public string TemplateName { get; set; }
        public object Model { get; set; }
    }
}