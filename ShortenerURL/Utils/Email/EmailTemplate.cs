﻿namespace ShortenerURL.Utils.Email
{
    public static class EmailTemplate
    {
        public static string AccountHasBeenDelete => "AccountHasBeenDelete";
        public static string EmailConfirmation => "EmailConfirmation";
        public static string EmailConfirmationAfterChange => "EmailConfirmationAfterChange";
        public static string EmailHasBeenUpdated => "EmailHasBeenUpdated";
        public static string PasswordHasBeenChanged => "PasswordHasBeenChanged";
        public static string PasswordRecovery => "PasswordRecovery";
        public static string SuccessfulConfirmation => "SuccessfulConfirmation";
    }
}