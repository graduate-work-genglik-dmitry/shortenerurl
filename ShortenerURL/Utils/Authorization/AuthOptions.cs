﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace ShortenerURL.Utils.Authorization
{
    public static class AuthOptions
    {
        private const string Issuer = "SURL";
        private const string Audience = "SURL_User";
        private const string Key = "i3v+Y0Dktj3Pi/yCy3ntt8Z+sPTi/PUu/w9gLrB9nQs=";
        public const int Lifetime = 7 * 1440; // Minutes

        private static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new(Encoding.ASCII.GetBytes(Key));
        }

        public static JwtSecurityToken GetJwtToken(ClaimsIdentity identity)
        {
            return new(
                issuer: Issuer,
                audience: Audience,
                notBefore: DateTime.UtcNow,
                claims: identity.Claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(Lifetime)),
                signingCredentials: new SigningCredentials(GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
        }

        public static Action<JwtBearerOptions> JwtBearerOptions { get; } = options =>
        {
            options.RequireHttpsMetadata = true;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = Issuer,

                ValidateAudience = true,
                ValidAudience = Audience,

                ValidateLifetime = true,

                IssuerSigningKey = GetSymmetricSecurityKey(),
                ValidateIssuerSigningKey = true,
            };
            options.Events = new JwtBearerEvents
            {
                OnMessageReceived = context =>
                {
                    if (context.Request.Cookies.ContainsKey("_au"))
                    {
                        context.Token = context.Request.Cookies["_au"];
                    }

                    return Task.CompletedTask;
                }
            };
        };
    }
}