﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace ShortenerURL.Utils.Authorization
{
    public static class AccountManager
    {
        public static Guid GetCurrentAccountId(ClaimsPrincipal user)
        {
            var id = user.Claims.FirstOrDefault(c => c.Type == CustomClaims.Id)?.Value ??
                     throw new InvalidOperationException("No claims");

            return Guid.Parse(id);
        }

        public static string GenerateNewPassword()
        {
            var length = GetRandomInteger(8, 14);
            var newPassword = string.Empty;

            for (var i = 0; i <= length; i++)
            {
                var ch = (char) GetRandomInteger(33, 126);
                if (char.IsLetterOrDigit(ch))
                {
                    newPassword += ch;
                    continue;
                }

                i--;
            }

            if (!ValidateUtil.PasswordValidator(newPassword))
            {
                const string alphabet = "abcdefghijklmnopqrstuvwxyz";

                var index = GetRandomInteger(0, 7);
                var tmpString = new StringBuilder(newPassword)
                {
                    [index] = $"{GetRandomInteger(0, 9).ToString()}".ToCharArray()[0],
                    [index + 1] = char.ToUpper(alphabet[GetRandomInteger(0, alphabet.Length)])
                };
                newPassword = tmpString.ToString();
            }

            return newPassword;
        }

        private static int GetRandomInteger(int min, int max)
        {
            var rng = new Random();
            return rng.Next(min, max);
        }
    }
}