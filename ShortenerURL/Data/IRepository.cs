﻿using ShortenerURL.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShortenerURL.Data
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<IList<TEntity>> GetAllAsync();
        Task<TEntity> CreateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task<TEntity> GetByIdAsync(Guid id);
    }
}