﻿using System.Threading.Tasks;
using ShortenerURL.Domain.AuthModels;

namespace ShortenerURL.Data
{
    public interface IAccountProfileRepository : IRepository<AccountProfileInfo>
    {
        Task<AccountProfileInfo> GetByUsernameAsync(string username);
        Task<AccountProfileInfo> GetByEmailAsync(string email);
    }
}