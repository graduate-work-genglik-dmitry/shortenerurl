﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using ShortenerURL.Domain.UrlModels;

namespace ShortenerURL.Data
{
    public interface IUrlRepository : IRepository<UrlDataInfo>
    {
        Task<IList<UrlDataInfo>> GetAllAsync(Guid accountId);
        Task<UrlDataInfo> GetByIdAsync(Guid accountId, Guid urlId);
        Task<UrlDataInfo> GetByShortUrlIdAsync(string shortUrlId);
        Task AddStatisticsAsync(string shortUrlId, UrlStatisticsInfo statisticsInfo);
        Task DeleteAllUrlsOnAccountAsync(Guid accountId);
        Task DeleteAllExpiredUrlsOnAccountAsync(Guid accountId);
        Task DeleteAllBlockDisposableUrlsOnAccountAsync(Guid accountId);

        Task<IList<UrlDataInfo>> GetRangeAsync(Guid accountId, SortDefinition<UrlDataInfo> sortDefinition,
            int skip, int limit);

        Task<long> GetUrlsCountOnAccount(Guid accountId);
    }
}