import { AfterViewInit, Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppConfig } from '../../../app-config';
import { ShortUrlService } from '../../../services/short-url.service';
import { AccessMode, UrlDataInfo, UrlStatisticsInfo } from '../../../models/short-url';
import { TranslateManager } from '../../../utils/translate-manager';
import { LocalizationService } from '../../../services/localization.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmDeleteDialog } from '../../manage/dialogs/confirm-delete-dialog/confirm-delete-dialog';
import { FindUrlErrorDialog } from '../find-url-error-dialog/find-url-error-dialog';
import { Place } from '../../../utils/enums';
import { UrlUpdateDialog } from '../url-update-dialog/url-update-dialog';
import { Router } from '@angular/router';

@Component({
    selector: 'app-url-info-dialog',
    templateUrl: './url-info-dialog.html',
    styleUrls: ['./url-info-dialog.scss']
})
// tslint:disable-next-line:component-class-suffix
export class UrlInfoDialog implements OnInit, OnDestroy, AfterViewInit {
    public url: UrlDataInfo;
    public deleteOrUpdate = false;
    public dataSource = new MatTableDataSource<UrlStatisticsInfo>();

    public get accessMode(): typeof AccessMode {
        return AccessMode;
    }

    displayedColumns: string[] = ['number-column', 'time-of-visit', 'ip', 'geo', 'device', 'os'];

    constructor(public dialogRef: MatDialogRef<UrlInfoDialog>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                public config: AppConfig,
                private urlService: ShortUrlService,
                public translate: LocalizationService,
                private dialog: MatDialog,
                private router: Router
    ) {
        dialogRef.disableClose = true;
    }

    @ViewChild(MatPaginator) paginator: MatPaginator;

    public ngAfterViewInit(): void {
        this.dataSource.paginator = this.paginator;
    }

    public ngOnInit(): void {
        this.url = this.data.url;
        this.dataSource = new MatTableDataSource<UrlStatisticsInfo>(this.url.statisticsInfo);
    }

    public ngOnDestroy(): void {
        this.dialogRef.close(this.deleteOrUpdate);
    }

    public getLocale(): string {
        return TranslateManager.CurrentLang;
    }

    public checkExpired(): boolean {
        return this.url.mode === 3 && this.url.clicks >= 1
            || this.url.expiration != null && new Date().getTime() > new Date(this.url.expiration).getTime();
    }

    public deleteUrl(): void {
        this.dialog.open(ConfirmDeleteDialog, { data: { url: this.url, mode: 'single' } }).afterClosed().subscribe(res => {
            if (res) {
                this.deleteOrUpdate = true;
                this.dialogRef.close(this.deleteOrUpdate);
            }
        });
    }

    public updateUrl(): void {
        this.dialog.open(UrlUpdateDialog, { data: { url: this.url }, autoFocus: false }).afterClosed().subscribe(res => {
            if (res) {
                this.deleteOrUpdate = true;

                this.urlService.find(this.url.id).subscribe(urlInfo => {
                    this.url = urlInfo;
                    this.dataSource = new MatTableDataSource<UrlStatisticsInfo>(urlInfo.statisticsInfo);
                    this.dataSource.paginator = this.paginator;
                }, () => {
                    this.dialog.open(FindUrlErrorDialog).afterClosed().subscribe(() => {
                        this.router.navigateByUrl(Place.Manage, { state: { needUpdate: true } }).finally();
                    });
                });
            }
        });
    }
}
