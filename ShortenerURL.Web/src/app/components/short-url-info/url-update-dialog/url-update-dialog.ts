import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppConfig } from '../../../app-config';
import { ShortUrlService } from '../../../services/short-url.service';
import { AccessMode, UpdateUrl, UrlDataInfo } from '../../../models/short-url';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../../utils/custom-validators';
import { TranslateManager } from '../../../utils/translate-manager';
import { Language } from '../../../utils/enums';
import { DateAdapter } from '@angular/material/core';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { SmthWentWrongDialog } from '../../manage/dialogs/smth-went-wrong-dialog/smth-went-wrong-dialog';

@Component({
    selector: 'app-url-update-dialog',
    templateUrl: './url-update-dialog.html',
    styleUrls: ['./url-update-dialog.scss'],
})
// tslint:disable-next-line:component-class-suffix
export class UrlUpdateDialog implements OnInit, OnDestroy {

    @ViewChild(MatSlideToggle) public toggle: MatSlideToggle;

    public url: UrlDataInfo;
    public onUpdate = false;
    public passwordHide = true;
    public form: FormGroup;
    public lang = TranslateManager.CurrentLang;

    private newExpirationDate: moment.Moment;
    public dateDurationInMinutes: number;
    public lowLifetime = false;
    public lifetimeNeedReduce = false;
    public invalidSecrets = false;
    public disabledButton = false;
    public error = false;
    public disableData = false;
    public checkboxDisabled = false;

    public minutes = new Array<string>();
    public hours = new Array<string>();

    public updateData: UpdateUrl = new UpdateUrl();

    public get accessMode(): typeof AccessMode {
        return AccessMode;
    }

    constructor(public dialogRef: MatDialogRef<UrlUpdateDialog>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                public config: AppConfig,
                private urlService: ShortUrlService,
                private formBuilder: FormBuilder,
                private dateAdapter: DateAdapter<Date>,
                private dialog: MatDialog) {
        this.dateAdapter.setLocale(this.lang);
        this.form = formBuilder.group({
            dateField: [moment(), [CustomValidators.urlExpirationDate]],
            password: ['', [CustomValidators.password]],
            secretQuestionText: ['', [Validators.maxLength(200)]],
            secretQuestion: ['', [Validators.maxLength(200)]],
            hours: ['09'],
            minutes: ['30'],
            halfDay: ['AM'],
            oldDateCheckbox: [false]
        });

        this.timeDigitFiller();
    }

    public myFilter = (date: moment.Moment): boolean => {
        const start = moment().add(-1, 'days').set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        const end = moment().add(30, 'days').set({ hour: 23, minute: 58 });
        return date > start && date <= end;
    }

    public ngOnInit(): void {
        this.url = this.data.url;
        this.updateData.urlId = this.url.id;
        this.updateData.isEternal = this.url.isEternal;
        if (!this.url.isEternal) {
            const expiredDate = moment(this.url.expiration);
            this.form.controls.dateField.setValue(moment() > expiredDate ? moment() : expiredDate);
        }
        this.checkDate();
    }

    public ngOnDestroy(): void {
        this.dialogRef.close(this.onUpdate);
    }

    public toggleChange(event: any): void {
        this.updateData.isEternal = !!event.checked;

        this.checkDate();
    }

    public checkboxChange(event: any): void {
        this.disableData = !!event.checked;

        this.checkDate();
    }

    public updateUrl(): void {
        this.checkDate();

        if (this.form.invalid || this.disabledButton) {
            return;
        }

        this.updateData.addedLifetime = this.lifetimeNeedReduce || this.disableData ? null : this.dateDurationInMinutes;
        this.updateData.reducedLifetime = this.lifetimeNeedReduce ? Math.abs(this.dateDurationInMinutes) : null;

        if (this.updateData.reducedLifetime === 0) {
            this.updateData.reducedLifetime = null;
        }

        if (this.updateData.addedLifetime === 0) {
            this.updateData.addedLifetime = null;
        }

        if (this.url.mode === AccessMode.Password) {
            const newPassword = this.form.controls.password.value;
            this.updateData.password = newPassword === null || newPassword.trim() === '' ? null : newPassword;
        }

        if (this.url.mode === AccessMode.SecretQuestion) {
            this.checkSecrets();

            this.updateData.secretQuestionText = this.invalidSecrets ? null : this.form.controls.secretQuestionText.value;
            this.updateData.secretQuestion = this.invalidSecrets ? null : this.form.controls.secretQuestion.value;
        }

        if (this.updateData.reducedLifetime === null && this.updateData.addedLifetime === null) {
            this.updateData.addedLifetime = 0;
        }

        this.urlService.update(this.updateData).subscribe(() => {
            this.onUpdate = true;
            this.dialogRef.close();
        }, () => {
            this.onUpdate = false;
            this.dialog.open(SmthWentWrongDialog).afterClosed().subscribe();
        });

    }

    public checkSecrets(): void {
        const newSecret = this.form.controls.secretQuestionText.value;
        const newAnswer = this.form.controls.secretQuestion.value;

        this.invalidSecrets = newSecret === null || newSecret.trim() === '' || newAnswer === null || newAnswer.trim() === '';
    }

    public timeDigitFiller(): void {
        if (this.lang === Language.Russian) {
            for (let i = 0; i < 24; i++) {
                this.hours.push(i < 10 ? `0${ i }` : `${ i }`);
            }
        }

        if (this.lang === Language.English) {
            for (let i = 1; i <= 12; i++) {
                this.hours.push(i < 10 ? `0${ i }` : `${ i }`);
            }
        }

        for (let i = 0; i < 60; i = i + 5) {
            this.minutes.push(i < 10 ? `0${ i }` : `${ i }`);
        }
    }

    public checkDate(): void {
        let hour = this.form.controls.hours.value;

        if (this.lang === Language.English) {
            hour = moment(`${ hour } ${ this.form.controls.halfDay.value }`, 'hh a').format('HH');
        }

        const minute = this.form.controls.minutes.value;
        this.newExpirationDate = moment(this.form.controls.dateField.value).set({
            hour,
            minute,
            millisecond: 0
        }).add(0 - moment(this.url.expiration).seconds(), 'seconds');


        if (this.url.isEternal) {
            this.dateDurationInMinutes = this.newExpirationDate.diff(moment().set({ second: 0, millisecond: 0 }), 'minutes');
        } else {
            this.dateDurationInMinutes = this.newExpirationDate.diff(this.url.expiration, 'minutes');
        }
        this.disabledButton = this.updateData.isEternal ? false : Math.abs(this.dateDurationInMinutes) < 15;
        if (moment(this.url.audit.createdOn) > this.newExpirationDate && !this.updateData.isEternal) {
            this.disabledButton = true;
        }
        if (this.disableData === true) {
            this.disabledButton = false;
        }

        const newLifetime = this.url.lifetime + this.dateDurationInMinutes;
        this.lifetimeNeedReduce = this.dateDurationInMinutes < 0;
        this.lowLifetime = newLifetime < 15;
    }

    public displayValidationText(controlName: string): string {
        const control = this.form.controls[controlName];
        const error = control.errors;

        this.error = true;

        if (this.form.controls.secretQuestionText.value.trim() === '' && this.form.controls.secretQuestion.value.trim() !== '') {
            return 'secret';
        }

        if (this.form.controls.secretQuestionText.value.trim() !== '' && this.form.controls.secretQuestion.value.trim() === '') {
            return 'secret';
        }

        if (error === null) {
            this.error = false;
            return '';
        }

        if (error.required) {
            return 'required';
        }

        if (error.invalidPassword) {
            return 'password';
        }

        if (error.maxlength) {
            return 'max';
        }

        if (this.disableData) {
            this.error = false;
            return '';
        }

        if (error.urlExpirationLate) {
            return 'late';
        }

        if (error.urlExpirationEarly) {
            return 'early';
        }

        if (error.invalidDate) {
            return 'date';
        }

        return;
    }
}


