import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShortUrlService } from '../../services/short-url.service';
import { UrlDataInfo } from '../../models/short-url';
import { MatDialog } from '@angular/material/dialog';
import { Place } from '../../utils/enums';
import { UrlInfoDialog } from './url-info-dialog/url-info-dialog';
import { FindUrlErrorDialog } from './find-url-error-dialog/find-url-error-dialog';

@Component({
    selector: 'app-short-url-info',
    template: ``,
})
export class ShortUrlInfoComponent implements OnInit {

    public id: string;
    public url: UrlDataInfo;

    constructor(private activateRoute: ActivatedRoute,
                private urlService: ShortUrlService,
                private dialog: MatDialog,
                private router: Router) {
        activateRoute.params.subscribe(params => this.id = params.id);
    }

    public ngOnInit(): void {
        this.urlService.find(this.id).subscribe(urlInfo => {
            this.url = urlInfo;
            this.dialog.open(UrlInfoDialog, { data: { url: this.url } }).afterClosed().subscribe(needUpdate => {
                this.router.navigateByUrl(Place.Manage, { state: { needUpdate } }).finally();
            });
        }, () => {
            this.dialog.open(FindUrlErrorDialog).afterClosed().subscribe(() => {
                this.router.navigateByUrl(Place.Manage, { state: { needUpdate: true } }).finally();
            });
        });
    }
}
