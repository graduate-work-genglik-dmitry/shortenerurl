import { Component } from '@angular/core';

@Component({
    selector: 'app-unsuccessful-registration-dialog',
    templateUrl: './failed-registration-dialog.html',
    styleUrls: ['./failed-registration-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class FailedRegistrationDialog {
}
