import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { LocalizationService } from '../../../../services/localization.service';
import { Language } from '../../../../utils/enums';

@Component({
    selector: 'app-change-language-dialog',
    templateUrl: './change-language-dialog.html',
    styleUrls: ['./change-language-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class ChangeLanguageDialog {

    public get Language(): typeof Language {
        return Language;
    }

    constructor(private bottomSheetRef: MatBottomSheetRef<ChangeLanguageDialog>,
                private localization: LocalizationService) {
    }

    changeLang(lang: Language): void {
        this.localization.useLang(lang);
        this.bottomSheetRef.dismiss();
    }

}
