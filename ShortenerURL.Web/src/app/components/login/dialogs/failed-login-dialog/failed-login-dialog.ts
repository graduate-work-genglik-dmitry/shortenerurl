import { Component } from '@angular/core';

@Component({
    selector: 'app-unsuccessful-login-dialog',
    templateUrl: './failed-login-dialog.html',
    styleUrls: ['./failed-login-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class FailedLoginDialog {
}
