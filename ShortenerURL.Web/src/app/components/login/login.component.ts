import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';
import { AuthManager } from '../../utils/auth-manager';
import { LoginModel } from '../../models/login.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../utils/custom-validators';
import { AccountProfileWithConfirmation } from '../../models/account';
import { MatDialog } from '@angular/material/dialog';
import { SuccessfulRegistrationDialog } from './dialogs/successful-registration-dialog/successful-registration-dialog';
import { ComponentType } from '@angular/cdk/overlay';
import { FailedRegistrationDialog } from './dialogs/failed-registration-dialog/failed-registration-dialog';
import { SuccessfulPasswordRecoveryDialog } from './dialogs/successful-password-recovery-dialog/successful-password-recovery-dialog';
import { FailedPasswordRecoveryDialog } from './dialogs/failed-password-recovery-dialog/failed-password-recovery-dialog';
import { FailedLoginDialog } from './dialogs/failed-login-dialog/failed-login-dialog';
import { UnconfirmedLoginDialog } from './dialogs/unconfirmed-login-dialog/unconfirmed-login-dialog';
import { LocalizationService } from '../../services/localization.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ChangeLanguageDialog } from './dialogs/change-language-dialog/change-language-dialog';
import { Place } from '../../utils/enums';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

    private loginModel: LoginModel;
    private registerModel: AccountProfileWithConfirmation;

    public form: FormGroup;
    public passwordHide = true;
    public isRegistration = false;
    public isPasswordRecovery = false;
    public isLoading = false;

    constructor(private accountService: AccountService,
                private router: Router,
                private formBuilder: FormBuilder,
                private dialog: MatDialog,
                public localization: LocalizationService,
                private bottomSheet: MatBottomSheet) {

        this.form = formBuilder.group({
            usernameOrEmail: ['', [Validators.required]],
            password: ['', [Validators.required, CustomValidators.password]]
        }, { validator: CustomValidators.equality() });
    }

    public ngOnInit(): void {
        if (AuthManager.Authenticated) {
            this.router.navigateByUrl(Place.Manage).finally();
        }
    }

    public toRegistration(): void {
        this.isRegistration = true;
        this.form.addControl('repeatPassword', new FormControl('',
            [Validators.required, CustomValidators.password]));
        this.form.addControl('email', new FormControl('',
            [Validators.required, Validators.email]));
        this.form.addControl('username', new FormControl('',
            [Validators.required, CustomValidators.username]));
        this.form.removeControl('usernameOrEmail');
        this.form.updateValueAndValidity();
    }

    public returnToLogin(): void {
        this.isRegistration = false;
        this.isPasswordRecovery = false;
        this.form.removeControl('repeatPassword');
        this.form.removeControl('email');
        this.form.removeControl('username');
        this.form.addControl('usernameOrEmail', new FormControl('',
            [Validators.required]));

        if (!this.form.contains('password')) {
            this.form.addControl('password', new FormControl('',
                [Validators.required, CustomValidators.password]));
        }

        this.form.updateValueAndValidity();
    }

    public displayValidationText(controlName: string): string {
        const control = this.form.controls[controlName];

        if (control.errors === null) {
            return '';
        }

        if (control.errors.required) {
            return 'required';
        }

        if (control.errors.invalidUsername) {
            return 'username';
        }

        if (control.errors.email) {
            return 'email';
        }

        if (control.errors.invalidPassword) {
            return 'password';
        }

        if (control.errors.notEquivalent) {
            return 'notEquivalent';
        }

        return;
    }

    public openDialog(dialog: ComponentType<unknown>): void {
        const dialogRef = this.dialog.open(dialog);

        dialogRef.afterClosed().subscribe();
    }

    public openBottomSheet(): void {
        this.bottomSheet.open(ChangeLanguageDialog);
    }

    public registration(): void {
        if (this.form.invalid) {
            return;
        }

        this.registerModel = {
            username: this.form.controls.username.value,
            email: this.form.controls.email.value,
            password: this.form.controls.password.value,
            repeatPassword: this.form.controls.repeatPassword.value
        };

        this.isLoading = true;
        this.accountService.create(this.registerModel).subscribe(() => {
            this.openDialog(SuccessfulRegistrationDialog);
            this.isLoading = false;
            this.returnToLogin();
        }, () => {
            this.openDialog(FailedRegistrationDialog);
            this.isLoading = false;
        });
    }

    public login(): void {
        if (this.form.invalid) {
            return;
        }

        this.loginModel = {
            usernameOrEmail: this.form.controls.usernameOrEmail.value,
            password: this.form.controls.password.value,
            email: /\S+@\S+\.\S+/.test(this.form.controls.usernameOrEmail.value)
        };

        this.isLoading = true;
        this.accountService.login(this.loginModel).subscribe(() => {
                this.isLoading = false;
                AuthManager.Authenticated = true;
                this.router.navigateByUrl(Place.Manage).finally();

                this.accountService.profile().subscribe(profile => {
                        AuthManager.Profile = profile;

                        if (!profile.accessIsAllowed) {
                            this.accountService.logout().subscribe(() => {
                                AuthManager.Authenticated = false;
                                AuthManager.Profile = null;
                                this.router.navigateByUrl(Place.Login).finally();
                                this.openDialog(UnconfirmedLoginDialog);
                            });
                        }
                    }
                );

            },
            () => {
                this.openDialog(FailedLoginDialog);
                this.isLoading = false;
            });
    }

    public forgotPassword(): void {
        this.isPasswordRecovery = true;

        this.form.removeControl('usernameOrEmail');
        this.form.removeControl('password');
        this.form.addControl('email', new FormControl('',
            [Validators.required, Validators.email]));
        this.form.updateValueAndValidity();
    }

    public passwordRecovery(): void {
        const emailField = this.form.controls.email;

        if (emailField.invalid) {
            return;
        }

        this.isLoading = true;
        this.accountService.passwordRecovery({ email: emailField.value }).subscribe(() => {
            this.openDialog(SuccessfulPasswordRecoveryDialog);
            this.returnToLogin();
            this.isLoading = false;
        }, () => {
            this.openDialog(FailedPasswordRecoveryDialog);
            this.isLoading = false;
        });
    }
}
