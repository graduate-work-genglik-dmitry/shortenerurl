import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppConfig } from '../../../../app-config';
import { ShortUrlService } from '../../../../services/short-url.service';

@Component({
    selector: 'app-confirm-delete-dialog',
    templateUrl: './confirm-delete-dialog.html',
    styleUrls: ['./confirm-delete-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class ConfirmDeleteDialog implements OnInit {

    public isDelete = false;
    public mode: 'single' | 'all' | 'expired' | 'blockDisposable';

    constructor(public dialogRef: MatDialogRef<ConfirmDeleteDialog>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                public config: AppConfig,
                private urlService: ShortUrlService
    ) {
    }

    public ngOnInit(): void {
        this.mode = this.data.mode;
    }

    public delete(): void {

        if (this.mode === 'single') {
            this.urlService.delete(this.data.url.id).subscribe(() => {
                this.dialogRef.close(true);
            });
        }

        if (this.mode === 'all') {
            this.urlService.deleteAll().subscribe(() => {
                this.dialogRef.close(true);
            });
        }

        if (this.mode === 'blockDisposable') {
            this.urlService.deleteAllBlockDisposable().subscribe(() => {
                this.dialogRef.close(true);
            });
        }

        if (this.mode === 'expired') {
            this.urlService.deleteAllExpired().subscribe(() => {
                this.dialogRef.close(true);
            });
        }
    }

}
