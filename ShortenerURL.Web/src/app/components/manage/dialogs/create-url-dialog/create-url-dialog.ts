import { Component, DoCheck, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ShortUrlService } from '../../../../services/short-url.service';
import * as moment from 'moment';
import { CustomValidators } from '../../../../utils/custom-validators';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { TranslateManager } from '../../../../utils/translate-manager';
import { Language } from '../../../../utils/enums';
import { AccessMode, NewUrl } from '../../../../models/short-url';
import { LocalizationService } from '../../../../services/localization.service';
import { SmthWentWrongDialog } from '../smth-went-wrong-dialog/smth-went-wrong-dialog';

@Component({
    selector: 'app-create-url-dialog',
    templateUrl: './create-url-dialog.html',
    styleUrls: ['./create-url-dialog.scss']
})
// tslint:disable-next-line:component-class-suffix
export class CreateUrlDialog implements OnInit, OnDestroy, DoCheck {

    public isCreate = false;
    public form: FormGroup;
    public lang = TranslateManager.CurrentLang;
    public minutes = new Array<string>();
    public hours = new Array<string>();
    public passwordHide = true;
    public currentMode: AccessMode = this.accessMode.Free;
    public newUrl = new NewUrl();
    public error = false;
    private expirationDate: moment.Moment;
    public lifetime: number;
    public lowLifetime = false;
    public bigLifetime = false;

    public modeLabelsArray = new Array<string>();

    public get accessMode(): typeof AccessMode {
        return AccessMode;
    }

    constructor(public dialogRef: MatDialogRef<CreateUrlDialog>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private urlService: ShortUrlService,
                private formBuilder: FormBuilder,
                private dateAdapter: DateAdapter<Date>,
                private localeService: LocalizationService,
                private dialog: MatDialog) {
        this.dateAdapter.setLocale(this.lang);
        this.form = formBuilder.group({
            dateField: [moment(), [CustomValidators.urlExpirationDate]],
            hours: ['09'],
            minutes: ['30'],
            halfDay: ['AM'],
            sourceUrl: ['https://', [Validators.required, CustomValidators.url]],
            mode: [0]
        });

        this.timeDigitFiller();
    }

    public ngOnInit(): void {
        this.getModeLabelsArray();
        this.checkDate();
    }

    public ngDoCheck(): void {
        if (this.currentMode === AccessMode.Free || this.currentMode === AccessMode.Disposable) {
            this.form.removeControl('password');
            this.form.removeControl('secretQuestionText');
            this.form.removeControl('secretQuestion');
            this.form.updateValueAndValidity();
        }

        if (this.currentMode === AccessMode.Password) {
            this.form.addControl('password', new FormControl('',
                [Validators.required, CustomValidators.password]));
            this.form.removeControl('secretQuestionText');
            this.form.removeControl('secretQuestion');
            this.form.updateValueAndValidity();
        }

        if (this.currentMode === AccessMode.SecretQuestion) {
            this.form.addControl('secretQuestionText', new FormControl('',
                [Validators.required, Validators.maxLength(200)]));
            this.form.addControl('secretQuestion', new FormControl('',
                [Validators.required, Validators.maxLength(200)]));
            this.form.removeControl('password');
            this.form.updateValueAndValidity();
        }
    }

    public ngOnDestroy(): void {
        this.dialogRef.close(this.isCreate);
    }

    public changeCurrentMode(): void {
        switch (this.form.controls.mode.value) {
            case 0:
                this.currentMode = AccessMode.Free;
                break;
            case 1:
                this.currentMode = AccessMode.SecretQuestion;
                break;
            case 2:
                this.currentMode = AccessMode.Password;
                break;
            case 3:
                this.currentMode = AccessMode.Disposable;
                break;
        }
    }

    public myFilter = (date: moment.Moment): boolean => {
        const start = moment().add(-1, 'days').set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        const end = moment().add(30, 'days').set({ hour: 23, minute: 58 });
        return date > start && date <= end;
    }

    public getModeLabelsArray(): void {
        this.modeLabelsArray.push(this.localeService.getLocaleKeyValue('TEXT_INFO.URL_MODE.FREE'));
        const secretLabel = this.localeService.getLocaleKeyValue('TEXT_INFO.URL_MODE.SECRET');
        this.modeLabelsArray.push(secretLabel.substring(0, secretLabel.length - 2));
        this.modeLabelsArray.push(this.localeService.getLocaleKeyValue('TEXT_INFO.URL_MODE.PASSWORD'));
        this.modeLabelsArray.push(this.localeService.getLocaleKeyValue('TEXT_INFO.URL_MODE.DISPOSABLE'));
    }

    public toggleChange(event: any): void {
        this.newUrl.isEternal = !!event.checked;
    }

    public createUrl(): void {

        if (this.form.invalid) {
            return;
        }

        this.newUrl.mode = this.currentMode;
        this.newUrl.sourceUrl = this.form.controls.sourceUrl.value;
        this.newUrl.lifetime = this.newUrl.isEternal ? 16 : this.lifetime;

        switch (this.currentMode) {
            case AccessMode.Disposable:
                this.newUrl.lifetime = 16;
                break;
            case AccessMode.Password:
                this.newUrl.passwordOrSecret = this.form.controls.password.value;
                break;
            case AccessMode.SecretQuestion:
                this.newUrl.passwordOrSecret = this.form.controls.secretQuestion.value;
                this.newUrl.secretQuestionText = this.form.controls.secretQuestionText.value;
                break;
        }

        this.urlService.create(this.newUrl).subscribe(() => {
            this.isCreate = true;
            this.dialogRef.close(this.isCreate);
        }, () => {
            this.isCreate = false;
            this.dialog.open(SmthWentWrongDialog).afterClosed().subscribe();
        });
    }

    public timeDigitFiller(): void {
        if (this.lang === Language.Russian) {
            for (let i = 0; i < 24; i++) {
                this.hours.push(i < 10 ? `0${ i }` : `${ i }`);
            }
        }

        if (this.lang === Language.English) {
            for (let i = 1; i <= 12; i++) {
                this.hours.push(i < 10 ? `0${ i }` : `${ i }`);
            }
        }

        for (let i = 0; i < 60; i = i + 5) {
            this.minutes.push(i < 10 ? `0${ i }` : `${ i }`);
        }
    }

    public secretChecker(): string {
        if (this.form.controls.secretQuestionText.value.trim() === '' || this.form.controls.secretQuestion.value.trim() === '') {
            return 'secret';
        }

        return;
    }

    public displayValidationText(controlName: string): string {
        const control = this.form.controls[controlName];
        const error = control.errors;

        this.error = true;

        if (error === null) {
            this.error = false;
            return '';
        }

        if (error.required) {
            return 'required';
        }

        if (error.invalidUrl) {
            return 'url';
        }

        if (error.invalidPassword) {
            return 'password';
        }

        if (error.maxlength) {
            return 'max';
        }

        if (error.urlExpirationLate) {
            return 'late';
        }

        if (error.urlExpirationEarly) {
            return 'early';
        }

        if (error.invalidDate) {
            return 'date';
        }

        return;
    }

    public checkDate(): void {
        let hour = this.form.controls.hours.value;

        if (this.lang === Language.English) {
            hour = moment(`${ hour } ${ this.form.controls.halfDay.value }`, 'hh a').format('HH');
        }

        const minute = this.form.controls.minutes.value;
        this.expirationDate = moment(this.form.controls.dateField.value).set({
            hour,
            minute,
            second: 0,
            millisecond: 0
        });


        this.lifetime = this.expirationDate.diff(moment().set({
            second: 0,
            millisecond: 0
        }), 'minutes');

        const newLifetime = this.lifetime;
        this.lowLifetime = newLifetime < 15;
        this.bigLifetime = newLifetime > 43200;
    }
}
