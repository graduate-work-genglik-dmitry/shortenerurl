import { Component, DoCheck, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AccountService } from '../../services/account.service';
import { AuthManager } from '../../utils/auth-manager';
import { ShortUrlService } from '../../services/short-url.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Place } from '../../utils/enums';
import { OrderMode, UrlDataInfo } from '../../models/short-url';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { TranslateManager } from '../../utils/translate-manager';
import { AppConfig } from '../../app-config';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/overlay';
import { ConfirmDeleteDialog } from './dialogs/confirm-delete-dialog/confirm-delete-dialog';
import { CreateUrlDialog } from './dialogs/create-url-dialog/create-url-dialog';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalizationService } from '../../services/localization.service';

@Component({
    selector: 'app-manage',
    templateUrl: './manage.component.html',
    styleUrls: ['./manage.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ManageComponent implements OnInit, DoCheck {
    public urlPaginateList: Array<UrlDataInfo> = [];

    public pageVisible = false;
    public menuIsOpen = false;
    public urlListIsLoading = false;

    public bottomButtonDisabled = false;

    // Pagination
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    public pageEvent: PageEvent;
    public startIndex = 0;
    public totalLength = 0;
    public pageSize = 10;
    public currentPageIndex = 0;
    public pageSizeVariables = [5, 10, 15, 50];
    public orderAsc = false;
    public orderMode: OrderMode = OrderMode.CreatedOn;
    public emptyPage = true;
    private state$: Observable<boolean>;

    public get Place(): typeof Place {
        return Place;
    }

    public get OrderMode(): typeof OrderMode {
        return OrderMode;
    }

    constructor(private accountService: AccountService,
                private urlService: ShortUrlService,
                private translate: LocalizationService,
                public appConfig: AppConfig,
                private dialog: MatDialog,
                private router: Router,
                public activatedRoute: ActivatedRoute) {
    }

    public ngOnInit(): void {
        this.paginateMover(0, this.orderMode, this.orderAsc);
    }

    public ngDoCheck(): void {
        this.state$ = this.activatedRoute.paramMap
            .pipe(map(() => window.history.state));
        if (window.history.state.needUpdate && !this.emptyPage) {
            this.paginateMover(this.currentPageIndex, this.orderMode, this.orderAsc);
            window.history.state.needUpdate = false;
        }
    }

    private paginateMover(pageIndex, orderMode: OrderMode, orderAsc: boolean): void {
        this.urlListIsLoading = false;
        this.urlService.paginate(pageIndex, this.pageSize, orderMode, orderAsc)
            .subscribe(paginateUrlList => {
                this.spinnerTimeout();
                this.urlPaginateList = paginateUrlList.pageItems;
                this.startIndex = paginateUrlList.globalStartIndex;
                this.pageVisible = true;
                this.totalLength = paginateUrlList.totalLength;
                this.emptyPage = this.totalLength === 0;
                this.bottomButtonDisabled = paginateUrlList.pageItems.length === 0;

                if (paginateUrlList.pageItems.length === 0) {
                    this.currentPageIndex = Math.max(this.currentPageIndex - 1, 0);
                    this.paginator.page.next({
                        pageIndex: this.currentPageIndex,
                        length: this.totalLength,
                        pageSize: this.pageSize
                    });
                } else {
                    this.currentPageIndex = pageIndex;
                }
            });
    }

    // To not twitch too much
    private spinnerTimeout(): void {
        window.setTimeout(() => {
            this.urlListIsLoading = true;
        }, 290);
    }

    public logout(): void {
        this.accountService.logout().subscribe(() => {
            AuthManager.Authenticated = false;
            AuthManager.Profile = null;
            this.router.navigateByUrl(Place.Login).finally();
        });
    }

    public menuNavigation(place: Place): void {
        this.menuIsOpen = false;
        this.router.navigateByUrl(place).finally();
        if (place === Place.Manage) {
            this.pageSize = 10;
            this.paginateMover(0, this.orderMode, this.orderAsc);
        }
    }

    private paginatorUpdater(pageIndex = 0, pageSize = 10): void {
        this.bottomButtonDisabled = this.urlPaginateList.length === 0;
        this.paginator.page.next({
            pageIndex,
            length: this.totalLength,
            pageSize
        });
    }

    public paginate(event?: PageEvent): PageEvent {
        this.urlListIsLoading = false;
        this.urlService.paginate(event.pageIndex, event.pageSize, this.orderMode, this.orderAsc)
            .subscribe(paginateUrlList => {
                this.spinnerTimeout();
                this.urlPaginateList = paginateUrlList.pageItems;
                this.startIndex = paginateUrlList.globalStartIndex;
                this.totalLength = paginateUrlList.totalLength;
                this.currentPageIndex = event.pageIndex;
                this.pageSize = event.pageSize;
                this.emptyPage = this.totalLength === 0;
                this.bottomButtonDisabled = paginateUrlList.pageItems.length === 0;
            });
        return event;
    }

    public sortChanger(order: OrderMode, orderAsc: boolean): void {
        this.orderAsc = orderAsc;
        this.orderMode = order;
        this.paginatorUpdater(this.currentPageIndex, this.pageSize);
    }

    public getLocale(): string {
        return TranslateManager.CurrentLang;
    }

    public urlParser(url: UrlDataInfo,
                     whatToParse: 'mode' | 'mode tooltip' | 'expiration' | 'is expired?'): string | boolean {

        if (whatToParse === 'mode') {
            return this.translate.getUrlAccessModeLabel(url);
        }

        if (whatToParse === 'expiration') {
            return this.translate.getUrlExpirationLabel(url);
        }


        if (whatToParse === 'is expired?') {
            return url.mode === 3 && url.clicks >= 1
                || url.expiration != null && new Date().getTime() > new Date(url.expiration).getTime();
        }

        if (whatToParse === 'mode tooltip') {
            return this.translate.getUrlAccessModeLabel(url, true);
        }
    }

    public deleteUrl(url?: UrlDataInfo, mode: 'single' | 'all' | 'expired' | 'blockDisposable' = 'single'): void {
        this.openDialog(ConfirmDeleteDialog, { url, mode }).afterClosed().subscribe(res => {
            if (res) {
                this.paginateMover(this.currentPageIndex, this.orderMode, this.orderAsc);
            }
        });
    }

    public createUrl(): void {
        this.openDialog(CreateUrlDialog).afterClosed().subscribe(res => {
            if (res) {
                this.paginateMover(this.currentPageIndex, this.orderMode, this.orderAsc);
            }
        });
    }

    public openUrlInfo(id: string): void {
        this.router.navigate([Place.ShortURL, id]).finally();
    }

    public goToSourceUrl(sourceUrl: string): void {
        window.open(sourceUrl, '_blank');
    }

    private openDialog(dialog: ComponentType<unknown>, data: any = null): MatDialogRef<unknown> {
        return this.dialog.open(dialog, {
            data
        });
    }
}
