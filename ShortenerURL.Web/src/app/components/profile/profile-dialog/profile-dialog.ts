import { Component, OnInit } from '@angular/core';
import { AccountService } from '../../../services/account.service';
import { LocalizationService } from '../../../services/localization.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthManager } from '../../../utils/auth-manager';
import { CustomValidators } from '../../../utils/custom-validators';
import { AccountProfileUpdate } from '../../../models/account';
import { ChangeLanguageDialog } from '../../login/dialogs/change-language-dialog/change-language-dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ProfileModel } from '../../../models/profile.model';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProfileConfirmUpdateDialog } from '../profile-confirm-update-dialog/profile-confirm-update-dialog';
import { ProfileErrorDialog } from '../profile-error-dialog/profile-error-dialog';
import { DeleteAccountDialog } from '../delete-account-dialog/delete-account-dialog';

@Component({
    selector: 'app-profile-dialog',
    templateUrl: './profile-dialog.html',
    styleUrls: ['./profile-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class ProfileDialog implements OnInit {

    public form: FormGroup;
    public profile = new ProfileModel();
    public updateProfileData = new AccountProfileUpdate();
    public passwordHide = true;
    public currentUsername: string;
    public currenEmail: string;
    public isLoading = false;

    constructor(private accountService: AccountService,
                private localizationService: LocalizationService,
                private formBuilder: FormBuilder,
                private bottomSheet: MatBottomSheet,
                private dialog: MatDialog,
                private dialogRef: MatDialogRef<ProfileDialog>
    ) {
        this.form = formBuilder.group({
            email: ['', [Validators.email, Validators.required]],
            username: ['', [Validators.required, CustomValidators.username]],
            currentPassword: ['', [Validators.required, CustomValidators.password]],
            newPassword: ['', [CustomValidators.password]]
        });
    }

    public ngOnInit(): void {
        this.accountService.profile().subscribe(profile => {
            AuthManager.Profile = profile;
            this.profile = profile;
            this.currenEmail = this.profile.email;
            this.currentUsername = this.profile.username;
            this.form.controls.email.setValue(this.profile.email);
            this.form.controls.username.setValue(this.profile.username);
            this.isLoading = true;
        });
    }

    public updateProfile(): void {

        if (this.form.invalid) {
            return;
        }

        this.updateProfileData.email = this.form.controls.email.value === this.currenEmail ?
            null : this.form.controls.email.value;

        this.updateProfileData.username = this.form.controls.username.value === this.currentUsername ?
            null : this.form.controls.username.value;

        this.updateProfileData.newPassword = this.form.controls.newPassword.value.trim() === '' ?
            null : this.form.controls.newPassword.value;

        this.updateProfileData.currentPassword = this.form.controls.currentPassword.value;

        this.isLoading = false;
        this.accountService.update(this.updateProfileData).subscribe(() => {
            this.updateSystemProfileInfo();
        }, () => {
            this.isLoading = true;
            this.dialog.open(ProfileErrorDialog);
        });
    }

    private updateSystemProfileInfo(): void {
        this.accountService.profile().subscribe(profile => {
            AuthManager.Profile = profile;
            this.profile = profile;
            this.form.controls.newPassword.setValue('');
            this.form.controls.currentPassword.setValue('');
            this.isLoading = true;
            this.currenEmail = this.profile.email;
            this.currentUsername = this.profile.username;
            this.dialog.open(ProfileConfirmUpdateDialog);
        });
    }

    public getConfirmedLabel(): string {
        return this.profile.confirmed ?
            this.localizationService.getLocaleKeyValue('TEXT_INFO.ACCOUNT_CONFIRMED') :
            this.localizationService.getLocaleKeyValue('TEXT_INFO.ACCOUNT_UNCONFIRMED');
    }

    public openBottomSheet(): void {
        this.bottomSheet.open(ChangeLanguageDialog);
    }

    public openDeleteProfileDialog(): void {
        this.dialog.open(DeleteAccountDialog).afterClosed().subscribe(res => {
            if (res) {
                this.dialogRef.close('delete');
            }
        });
    }

    public displayValidationText(controlName: string): string {
        const control = this.form.controls[controlName];
        const errors = control.errors;

        if (errors === null) {
            return '';
        }

        if (errors.required) {
            return 'required';
        }

        if (errors.invalidUsername) {
            return 'username';
        }

        if (errors.email) {
            return 'email';
        }

        if (errors.invalidPassword) {
            return 'password';
        }

        return;
    }

}
