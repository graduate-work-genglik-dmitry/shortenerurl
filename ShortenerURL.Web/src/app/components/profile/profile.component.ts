import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Place } from '../../utils/enums';
import { ProfileDialog } from './profile-dialog/profile-dialog';
import { AccountService } from '../../services/account.service';

@Component({
    selector: 'app-profile',
    template: ''
})
export class ProfileComponent implements OnInit {

    constructor(private dialog: MatDialog,
                private router: Router,
                private accountService: AccountService) {
    }

    ngOnInit(): void {
        this.dialog.open(ProfileDialog).afterClosed().subscribe(res => {
            if (res === 'delete') {
                this.accountService.logout().subscribe();
            } else {
                this.router.navigateByUrl(Place.Manage).finally();
            }
        });
    }

}
