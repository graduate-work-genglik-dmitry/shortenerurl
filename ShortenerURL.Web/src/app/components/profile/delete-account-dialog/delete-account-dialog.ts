import { Component } from '@angular/core';
import { AccountService } from '../../../services/account.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../../../utils/custom-validators';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-delete-account-dialog',
    templateUrl: './delete-account-dialog.html',
    styleUrls: ['./delete-account-dialog.scss']
})

// tslint:disable-next-line:component-class-suffix
export class DeleteAccountDialog {

    public form = new FormGroup({});
    public passwordHide = true;
    public deleteError = false;
    public isLoading = false;

    constructor(private accountService: AccountService,
                private dialogRef: MatDialogRef<DeleteAccountDialog>) {
        this.form.addControl('password',
            new FormControl('', [Validators.required, CustomValidators.password]));
    }

    // To not twitch too much
    private spinnerTimeout(): void {
        window.setTimeout(() => {
            this.isLoading = false;
        }, 290);
    }

    public deleteProfile(): void {
        if (this.form.invalid) {
            return;
        }

        this.deleteError = false;
        this.isLoading = true;
        this.accountService.delete(this.form.controls.password.value).subscribe(() => {
            this.dialogRef.close(true);
        }, () => {
            this.spinnerTimeout();
            this.deleteError = true;
        });
    }

    public displayValidationText(controlName: string): string {
        const control = this.form.controls[controlName];
        const errors = control.errors;

        if (errors === null) {
            return '';
        }

        if (errors.required) {
            return 'required';
        }

        if (errors.invalidPassword) {
            return 'password';
        }

        return;
    }

}
