import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ManageComponent } from './components/manage/manage.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ShortUrlInfoComponent } from './components/short-url-info/short-url-info.component';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './utils/auth.interceptor';
import { AuthManager } from './utils/auth-manager';
import { LoginGuard } from './guards/login.guard';
import { AppConfig } from './app-config';
import { PaginatorIntlService, TranslateManager } from './utils/translate-manager';

import { ShortUrlService } from './services/short-url.service';
import { AccountService } from './services/account.service';
import { LocalizationService } from './services/localization.service';
import { CookieService } from 'ngx-cookie-service';

import { SuccessfulRegistrationDialog } from './components/login/dialogs/successful-registration-dialog/successful-registration-dialog';
import { FailedRegistrationDialog } from './components/login/dialogs/failed-registration-dialog/failed-registration-dialog';
import { FailedPasswordRecoveryDialog } from './components/login/dialogs/failed-password-recovery-dialog/failed-password-recovery-dialog';
import { SuccessfulPasswordRecoveryDialog } from './components/login/dialogs/successful-password-recovery-dialog/successful-password-recovery-dialog';
import { FailedLoginDialog } from './components/login/dialogs/failed-login-dialog/failed-login-dialog';
import { UnconfirmedLoginDialog } from './components/login/dialogs/unconfirmed-login-dialog/unconfirmed-login-dialog';
import { ChangeLanguageDialog } from './components/login/dialogs/change-language-dialog/change-language-dialog';
import { ConfirmDeleteDialog } from './components/manage/dialogs/confirm-delete-dialog/confirm-delete-dialog';
import { CreateUrlDialog } from './components/manage/dialogs/create-url-dialog/create-url-dialog';
import { UrlInfoDialog } from './components/short-url-info/url-info-dialog/url-info-dialog';
import { FindUrlErrorDialog } from './components/short-url-info/find-url-error-dialog/find-url-error-dialog';
import { UrlUpdateDialog } from './components/short-url-info/url-update-dialog/url-update-dialog';
import { SmthWentWrongDialog } from './components/manage/dialogs/smth-went-wrong-dialog/smth-went-wrong-dialog';
import { ProfileDialog } from './components/profile/profile-dialog/profile-dialog';
import { ProfileConfirmUpdateDialog } from './components/profile/profile-confirm-update-dialog/profile-confirm-update-dialog';
import { ProfileErrorDialog } from './components/profile/profile-error-dialog/profile-error-dialog';
import { DeleteAccountDialog } from './components/profile/delete-account-dialog/delete-account-dialog';

import { Language } from './utils/enums';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';

registerLocaleData(localeRu, 'ru');

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ManageComponent,
        ProfileComponent,
        ShortUrlInfoComponent,
        SuccessfulRegistrationDialog,
        FailedRegistrationDialog,
        FailedPasswordRecoveryDialog,
        SuccessfulPasswordRecoveryDialog,
        FailedLoginDialog,
        UnconfirmedLoginDialog,
        ChangeLanguageDialog,
        ConfirmDeleteDialog,
        CreateUrlDialog,
        UrlInfoDialog,
        FindUrlErrorDialog,
        UrlUpdateDialog,
        SmthWentWrongDialog,
        ProfileDialog,
        ProfileConfirmUpdateDialog,
        ProfileErrorDialog,
        DeleteAccountDialog,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (TranslateManager.createTranslateLoader),
                deps: [HttpClient]
            },
            defaultLanguage: Language.English
        }),
        BrowserAnimationsModule,
        MatCardModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatBottomSheetModule,
        MatSidenavModule,
        MatListModule,
        MatTooltipModule,
        MatToolbarModule,
        MatRippleModule,
        MatProgressBarModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatMenuModule,
        MatButtonToggleModule,
        MatTableModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSlideToggleModule,
        MatSelectModule,
        FormsModule,
        MatCheckboxModule
    ],
    providers: [
        AppConfig,
        AccountService,
        ShortUrlService,
        LocalizationService,
        CookieService,
        LoginGuard,
        AuthManager,
        FormBuilder,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: MatPaginatorIntl,
            useFactory: (translate) => {
                const service = new PaginatorIntlService();
                service.injectTranslateService(translate);
                return service;
            },
            deps: [TranslateService]
        },
        {
            provide: LOCALE_ID,
            useValue: 'ru'
        },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE]
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: MAT_MOMENT_DATE_FORMATS
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
