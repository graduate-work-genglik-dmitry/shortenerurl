import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app-config';
import { Observable } from 'rxjs';
import { LoginModel } from '../models/login.model';
import { PasswordRecoveryModel } from '../models/password-recovery.model';
import { AccountProfileUpdate, AccountProfileWithConfirmation } from '../models/account';
import { ProfileModel } from '../models/profile.model';

@Injectable()
export class AccountService {

    private path = `${ this.appConfig.apiRoot }/account`;

    constructor(private http: HttpClient, private appConfig: AppConfig) {
    }

    public login(loginModel: LoginModel): Observable<void> {
        return this.http.post<void>(`${ this.path }/login`, loginModel);
    }

    public logout(): Observable<void> {
        return this.http.post<void>(`${ this.path }/logout`, null);
    }

    public check(): Observable<void> {
        return this.http.get<void>(`${ this.path }/check`);
    }

    // Account Management requests
    public create(newAccountData: AccountProfileWithConfirmation): Observable<void> {
        return this.http.post<void>(`${ this.path }/create`, newAccountData);
    }

    public delete(password: string): Observable<void> {
        return this.http.delete<void>(`${ this.path }/delete?password=${ password }`);
    }

    public profile(): Observable<ProfileModel> {
        return this.http.get<ProfileModel>(`${ this.path }/profile`);
    }

    public update(updateAccountData: AccountProfileUpdate): Observable<void> {
        return this.http.put<void>(`${ this.path }/update`, updateAccountData);
    }

    public passwordRecovery(email: PasswordRecoveryModel): Observable<void> {
        return this.http.post<void>(`${ this.path }/password-recovery`, email);
    }
}
