import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../app-config';
import { Observable } from 'rxjs';
import { NewUrl, OrderMode, UpdateUrl, UrlDataInfo, UrlPaginate } from '../models/short-url';

@Injectable()
export class ShortUrlService {
    private path = `${ this.appConfig.apiRoot }/url`;

    constructor(private http: HttpClient, private appConfig: AppConfig) {
    }

    public create(newUrl: NewUrl): Observable<UrlDataInfo> {
        return this.http.post<UrlDataInfo>(`${ this.path }/create`, newUrl);
    }

    public delete(urlId: string): Observable<void> {
        return this.http.delete<void>(`${ this.path }/delete?urlId=${ urlId }`);
    }

    public deleteAllExpired(): Observable<void> {
        return this.http.delete<void>(`${ this.path }/delete/all/expired`);
    }

    public deleteAllBlockDisposable(): Observable<void> {
        return this.http.delete<void>(`${ this.path }/delete/all/blocked-disposable`);
    }

    public deleteAll(): Observable<void> {
        return this.http.delete<void>(`${ this.path }/delete/all`);
    }

    public getAllForAccount(): Observable<UrlDataInfo[]> {
        return this.http.get<UrlDataInfo[]>(`${ this.path }/all`);
    }

    public find(urlId: string): Observable<UrlDataInfo> {
        return this.http.get<UrlDataInfo>(`${ this.path }/find/${ urlId }`);
    }

    public update(updateUrlData: UpdateUrl): Observable<UrlDataInfo> {
        return this.http.put<UrlDataInfo>(`${ this.path }/update`, updateUrlData);
    }

    public paginate(pageIndex: number, pageSize: number, orderMode: OrderMode, orderAsc = true): Observable<UrlPaginate> {
        return this.http.get<UrlPaginate>(`${ this.path }/paginate?` +
            `pageIndex=${ pageIndex }&` +
            `pageSize=${ pageSize }&` +
            `orderMode=${ orderMode }` +
            `&orderAsc=${ orderAsc }`
        );
    }
}
