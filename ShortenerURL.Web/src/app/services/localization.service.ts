import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { Language } from '../utils/enums';
import { TranslateManager } from '../utils/translate-manager';
import { AccessMode, UrlDataInfo } from '../models/short-url';

@Injectable({
    providedIn: 'root'
})
export class LocalizationService {

    constructor(private translate: TranslateService, private cookieService: CookieService) {
    }

    public useLang(lang: Language | string): void {
        this.translate.use(lang);
        this.cookieService.set('AppLocale', lang, {
            path: '/manage'
        });
        TranslateManager.CurrentLang = lang;
    }

    public setDefaultLangInCookie(): void {
        this.cookieService.set('AppLocale', Language.English, {
            path: '/manage'
        });
        TranslateManager.CurrentLang = Language.English;
    }

    public getLocaleKeyValue(key: string): string {
        let keyValue: string;
        this.translate.get(key).subscribe(value => {
            keyValue = value;
        });

        return keyValue;
    }

    public getUrlAccessModeLabel(url: UrlDataInfo, tooltip: boolean = false): string {
        let translateKeyModeValue: string;

        switch (url.mode) {
            case AccessMode.Free:
                translateKeyModeValue = this.getLocaleKeyValue(tooltip ? 'TOOLTIP.URL_MODE.FREE' : 'TEXT_INFO.URL_MODE.FREE');
                break;
            case AccessMode.SecretQuestion:
                translateKeyModeValue = this.getLocaleKeyValue(tooltip ? 'TOOLTIP.URL_MODE.SECRET' : 'TEXT_INFO.URL_MODE.SECRET');
                if (!tooltip) {
                    translateKeyModeValue += ` "${ url.secretQuestionText }"`;
                }
                break;
            case AccessMode.Disposable:
                translateKeyModeValue = this.getLocaleKeyValue(tooltip ? 'TOOLTIP.URL_MODE.DISPOSABLE' : 'TEXT_INFO.URL_MODE.DISPOSABLE');
                break;
            case AccessMode.Password:
                translateKeyModeValue = this.getLocaleKeyValue(tooltip ? 'TOOLTIP.URL_MODE.PASSWORD' : 'TEXT_INFO.URL_MODE.PASSWORD');
                break;
        }

        return translateKeyModeValue;
    }

    public getUrlExpirationLabel(url: UrlDataInfo): string {
        let translateKeyExpirationValue: string;

        if (url.mode === AccessMode.Disposable && url.clicks >= 1) {
            translateKeyExpirationValue = this.getLocaleKeyValue('TEXT_INFO.DISPOSABLE_BLOCK');
            return translateKeyExpirationValue;
        }

        if (url.expiration === null) {
            translateKeyExpirationValue = this.getLocaleKeyValue('TEXT_INFO.NOT_EXPIRED');
        } else if (new Date().getTime() > new Date(url.expiration).getTime()) {
            translateKeyExpirationValue = this.getLocaleKeyValue('TEXT_INFO.EXPIRED');
        } else {
            translateKeyExpirationValue = this.getLocaleKeyValue('TEXT_INFO.EXPIRATION');
        }

        return translateKeyExpirationValue;
    }
}
