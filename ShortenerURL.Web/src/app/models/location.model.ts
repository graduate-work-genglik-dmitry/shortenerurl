export class LocationModel {
    public status: string;
    public country: string;
    public regionName: string;
    public city: string;
}
