export class ProfileModel {
    public id: string;
    public username: string;
    public email: string;
    public confirmed: boolean;
    public accessIsAllowed: boolean;
}
