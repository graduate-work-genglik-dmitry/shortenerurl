export class AuditModel {
    public createdOn: Date;
    public modifiedOn: Date;
}
