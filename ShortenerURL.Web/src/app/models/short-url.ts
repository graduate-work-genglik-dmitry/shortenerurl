import { AuditModel } from './audit.model';

export enum AccessMode {
    Free,
    SecretQuestion,
    Password,
    Disposable
}

export enum OrderMode {
    ModifiedOn = 'ModifiedOn',
    CreatedOn = 'CreatedOn',
    Expiration = 'Expiration',
    RedirectCount = 'RedirectCount'
}

export class NewUrl {
    public sourceUrl: string;
    public mode: AccessMode;
    public lifetime: number | null;
    public passwordOrSecret: string;
    public secretQuestionText: string;
    public isEternal: boolean;
}

export class UpdateUrl {
    public urlId: string;
    public addedLifetime: number | null;
    public reducedLifetime: number | null;
    public password: string;
    public secretQuestion: string;
    public secretQuestionText: string;
    public isEternal: boolean | null;
}

export class UrlStatisticsInfo {
    public ip: string;
    public location: string;
    public device: string;
    public operationSystem: string;
    public timeOfVisit: Date;
}

export class UrlDataInfo {
    public accountId: string;

    public sourceUrl: string;
    public shortUrlId: string;
    public expiration: Date | null;
    public lifetime: number | null;
    public mode: AccessMode;

    public secretQuestion: string;
    public secretQuestionText: string;
    public password: string;
    public isDisposable: boolean | null;
    public isEternal: boolean | null;

    public statisticsInfo: Array<UrlStatisticsInfo>;
    public clicks: number;

    public id: string;
    public audit: AuditModel;
}

export class UrlPaginate {
    public totalLength: number;
    public pageIndex: number;
    public globalStartIndex: number | null;
    public pageItems: Array<UrlDataInfo>;
    public isEmpty: boolean;
}
