export class AccountProfileWithConfirmation {
    public username: string;
    public email: string;
    public password: string;
    public repeatPassword: string;
}

export class AccountProfileUpdate {
    public username: string;
    public email: string;
    public newPassword: string;
    public currentPassword: string;
}
