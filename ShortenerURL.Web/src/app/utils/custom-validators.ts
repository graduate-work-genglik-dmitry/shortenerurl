import { FormControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import * as moment from 'moment';

export class CustomValidators {
    private static readonly passwordRegex = /^(?=.*[A-Z])(?=.*\d)(?=.*[a-z])[\da-zA-Z(.,:;?!*+%-<>@[\]{}\/_{}$#)]{8,14}$/;
    private static readonly usernameRegex = /^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
    private static readonly dateRegex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    private static readonly urlRegex = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+(?:[a-z\u00a1-\uffff]{2,}\.?))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

    public static password = (control: FormControl) => {
        if (control.value.trim() !== '' && !CustomValidators.passwordRegex.test(control.value)) {
            return { invalidPassword: true };
        }
        return null;
    }

    public static username = (control: FormControl) => {
        if (!CustomValidators.usernameRegex.test(control.value)) {
            return { invalidUsername: true };
        }
        return null;
    }

    public static equality(fieldName1: string = 'password',
                           fieldName2: string = 'repeatPassword'): ValidatorFn {
        return (group: FormGroup): ValidationErrors | null => {
            const control1 = group.controls[fieldName1];
            const control2 = group.controls[fieldName2];

            if (!control1 || !control2) {
                return null;
            }

            if (control1.value !== control2.value) {
                control1.setErrors({ notEquivalent: true });
                control2.setErrors({ notEquivalent: true });
            } else if (control1.value === control2.value) {
                if (control1.valid || control2.valid) {
                    control1.setErrors(null);
                    control2.setErrors(null);
                }
            }
            return null;
        };
    }

    public static urlExpirationDate = (control: FormControl) => {
        const start = moment().add(-1, 'days');
        const end = moment().add(30, 'days').set({ hour: 23, minute: 58 });
        if (control.value > start && control.value <= end) {
            return null;
        }
        if (control.value < start) {
            return { urlExpirationEarly: true };
        }
        return { urlExpirationLate: true };
    }

    public static date = (control: FormControl) => {
        if (!CustomValidators.dateRegex.test(control.value)) {
            return { invalidDate: true };
        }
        return null;
    }

    public static url = (control: FormControl) => {
        if (!CustomValidators.urlRegex.test(control.value)) {
            return { invalidUrl: true };
        }
        return null;
    }
}
