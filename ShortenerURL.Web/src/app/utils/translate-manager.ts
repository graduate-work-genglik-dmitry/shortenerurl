import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';
import { Language } from './enums';

export class TranslateManager {
    public static CurrentLang: Language | string;

    public static createTranslateLoader(http: HttpClient): TranslateHttpLoader {
        return new TranslateHttpLoader(http, './assets/i18n/', '.json');
    }
}

export class PaginatorIntlService extends MatPaginatorIntl {

    public translate: TranslateService;
    public itemsPerPageLabel: string;
    public nextPageLabel: string;
    public previousPageLabel: string;
    public firstPageLabel: string;
    public lastPageLabel: string;

    public getRangeLabel = function(page, pageSize, length): string {
        let ofLabel: string;

        this.translate.get('PAGINATOR.LABEL.OF').subscribe(val => {
            ofLabel = val;
        });

        if (length === 0 || pageSize === 0) {
            return `0 ${ ofLabel } ${ length }`;
        }

        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        const endIndex = startIndex < length ?
            Math.min(startIndex + pageSize, length) :
            startIndex + pageSize;

        return `${ startIndex + 1 } - ${ endIndex } ${ ofLabel } ${ length }`;
    };

    public injectTranslateService(translate: TranslateService): void {
        this.translate = translate;

        this.translate.onLangChange.subscribe(() => {
            this.translateLabels();
        });

        this.translateLabels();
    }

    public translateLabels(): void {
        const keyArray = [
            'PAGINATOR.LABEL.ITEMS_PER_PAGE',
            'PAGINATOR.LABEL.NEXT_PAGE',
            'PAGINATOR.LABEL.PREVIOUS_PAGE',
            'PAGINATOR.LABEL.FIRST_PAGE',
            'PAGINATOR.LABEL.LAST_PAGE'
        ];

        this.translate.get(keyArray).subscribe(val => {
            this.itemsPerPageLabel = val['PAGINATOR.LABEL.ITEMS_PER_PAGE'];
            this.nextPageLabel = val['PAGINATOR.LABEL.NEXT_PAGE'];
            this.previousPageLabel = val['PAGINATOR.LABEL.PREVIOUS_PAGE'];
            this.firstPageLabel = val['PAGINATOR.LABEL.FIRST_PAGE'];
            this.lastPageLabel = val['PAGINATOR.LABEL.LAST_PAGE'];
        });
    }

}
