export enum Language {
    Russian = 'ru',
    English = 'en'
}

export enum Place {
    Manage = '/',
    Profile = 'profile',
    Login = 'login',
    ShortURL = 'surl'
}
