import { ProfileModel } from '../models/profile.model';

export class AuthManager {
    private static isAuthenticated = true;
    private static currentProfile: ProfileModel = null;

    public static get Authenticated(): boolean {
        return this.isAuthenticated;
    }

    public static set Authenticated(value: boolean) {
        this.isAuthenticated = value;
    }

    public static get Profile(): ProfileModel {
        return this.currentProfile;
    }

    public static set Profile(value: ProfileModel) {
        this.currentProfile = value;
    }
}
