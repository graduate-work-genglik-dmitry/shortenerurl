import { Injectable } from '@angular/core';

@Injectable()
export class AppConfig {

    private currentDomain = 'https://localhost:4444';

    public get serverRoot(): string {
        return this.currentDomain;
    }

    public get webAppRoot(): string {
        return `${ this.currentDomain }/manage`;
    }

    public get apiRoot(): string {
        return `${ this.currentDomain }/api`;
    }
}
