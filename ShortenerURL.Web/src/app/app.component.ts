import { Component, OnInit } from '@angular/core';
import { AccountService } from './services/account.service';
import { AuthManager } from './utils/auth-manager';
import { CookieService } from 'ngx-cookie-service';
import { LocalizationService } from './services/localization.service';
import { Language } from './utils/enums';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public title = 'shortener-url';

    constructor(private accountService: AccountService,
                private cookieService: CookieService,
                private localizationService: LocalizationService) {
    }

    public ngOnInit(): void {
        this.profileChecker();
        this.localeChecker();
    }

    private profileChecker(): void {
        this.accountService.profile().subscribe(profile => {
            AuthManager.Profile = profile;
        });
    }

    private localeChecker(): void {
        if (!this.cookieService.check('AppLocale')) {
            this.localizationService.setDefaultLangInCookie();
        } else {
            const lang = this.cookieService.get('AppLocale');

            if (Object.values<string>(Language).includes(lang)) {
                this.localizationService.useLang(lang);
            } else {
                this.localizationService.setDefaultLangInCookie();
            }
        }
    }
}


