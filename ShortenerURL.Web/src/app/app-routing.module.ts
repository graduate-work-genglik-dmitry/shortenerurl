import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageComponent } from './components/manage/manage.component';
import { LoginComponent } from './components/login/login.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ShortUrlInfoComponent } from './components/short-url-info/short-url-info.component';
import { LoginGuard } from './guards/login.guard';
import { Place } from './utils/enums';

const routes: Routes = [
    {
        path: '',
        component: ManageComponent,
        canActivate: [LoginGuard],
        canActivateChild: [LoginGuard],
        children: [
            { path: Place.Profile, component: ProfileComponent },
            { path: `${ Place.ShortURL }/:id`, component: ShortUrlInfoComponent },
            { path: Place.ShortURL, redirectTo: Place.Manage, pathMatch: 'full' }
        ]
    },
    { path: Place.Login, component: LoginComponent },
    { path: '**', redirectTo: Place.Manage }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
