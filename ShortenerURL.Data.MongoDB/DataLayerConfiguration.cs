﻿using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using ShortenerURL.Domain.UrlModels;

namespace ShortenerURL.Data.MongoDB
{
    public static class DataLayerConfiguration
    {
        private static readonly object Locker = new();
        public static bool Configured { get; private set; }

        public static IMongoDatabase GetDatabase(string connectionString)
        {
            var url = MongoUrl.Create(connectionString);
            var client = new MongoClient(url);
            return client.GetDatabase(url.DatabaseName);
        }

        public static void Configure()
        {
            if (Configured) return;

            lock (Locker)
            {
                if (Configured) return;
                ConfigureInternal();
                Configured = true;
            }
        }

        private static void ConfigureInternal()
        {
            BsonClassMap.RegisterClassMap<UrlDataInfo>(classMap =>
            {
                classMap.AutoMap();
            });
        }
    }
}
