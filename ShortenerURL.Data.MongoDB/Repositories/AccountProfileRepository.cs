﻿using System.Threading.Tasks;
using MongoDB.Driver;
using ShortenerURL.Domain.AuthModels;

namespace ShortenerURL.Data.MongoDB.Repositories
{
    public class AccountProfileRepository : DataRepository<AccountProfileInfo>, IAccountProfileRepository
    {
        public AccountProfileRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase, "AccountProfiles")
        {
        }

        public async Task<AccountProfileInfo> GetByUsernameAsync(string username)
        {
            return await MongoCollection.Find(entity => entity.Username == username).SingleOrDefaultAsync();
        }

        public async Task<AccountProfileInfo> GetByEmailAsync(string email)
        {
            return await MongoCollection.Find(entity => entity.Email == email).SingleOrDefaultAsync();
        }
    }
}