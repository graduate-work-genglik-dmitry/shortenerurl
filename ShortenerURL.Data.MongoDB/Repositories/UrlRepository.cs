﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShortenerURL.Domain.Enums;
using ShortenerURL.Domain.UrlModels;

namespace ShortenerURL.Data.MongoDB.Repositories
{
    public class UrlRepository : DataRepository<UrlDataInfo>, IUrlRepository
    {
        public UrlRepository(IMongoDatabase mongoDatabase) : base(mongoDatabase, "ShortUrls")
        {
        }

        public async Task<IList<UrlDataInfo>> GetAllAsync(Guid accountId)
        {
            return await MongoCollection.Find(entity => entity.AccountId == accountId).ToListAsync();
        }

        public async Task<UrlDataInfo> GetByIdAsync(Guid accountId, Guid urlId)
        {
            return await MongoCollection.Find(entity => entity.AccountId == accountId && entity.Id == urlId)
                .SingleOrDefaultAsync();
        }

        public async Task<UrlDataInfo> GetByShortUrlIdAsync(string shortUrlId)
        {
            return await MongoCollection.Find(entity => entity.ShortUrlId == shortUrlId).SingleOrDefaultAsync();
        }

        public async Task AddStatisticsAsync(string shortUrlId, UrlStatisticsInfo statisticsInfo)
        {
            var url = await GetByShortUrlIdAsync(shortUrlId);
            url.StatisticsInfo.Add(statisticsInfo);
            url.Clicks++;
            url.Audit.Modify();
            await MongoCollection.FindOneAndReplaceAsync(entity => entity.Id == url.Id, url);
        }

        public async Task DeleteAllUrlsOnAccountAsync(Guid accountId)
        {
            await MongoCollection.DeleteManyAsync(deleteEntity => deleteEntity.AccountId == accountId);
        }

        public async Task DeleteAllExpiredUrlsOnAccountAsync(Guid accountId)
        {
            await MongoCollection.DeleteManyAsync(deleteEntity =>
                deleteEntity.AccountId == accountId && deleteEntity.Expiration < DateTime.Now);
        }

        public async Task DeleteAllBlockDisposableUrlsOnAccountAsync(Guid accountId)
        {
            await MongoCollection.DeleteManyAsync(deleteEntity =>
                deleteEntity.AccountId == accountId && deleteEntity.Mode == AccessMode.Disposable &&
                deleteEntity.Clicks > 0);
        }

        public async Task<IList<UrlDataInfo>> GetRangeAsync(Guid accountId, SortDefinition<UrlDataInfo> sortDefinition,
            int skip, int limit)
        {
            return await MongoCollection.Find(entity => entity.AccountId == accountId).Sort(sortDefinition).Skip(skip)
                .Limit(limit).ToListAsync();
        }

        public async Task<long> GetUrlsCountOnAccount(Guid accountId)
        {
            return await MongoCollection.CountDocumentsAsync(url => url.AccountId == accountId);
        }
    }
}