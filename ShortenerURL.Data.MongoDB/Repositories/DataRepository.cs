﻿using MongoDB.Driver;
using ShortenerURL.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShortenerURL.Data.MongoDB.Repositories
{
    public abstract class DataRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected IMongoCollection<TEntity> MongoCollection { get; }
        protected DataRepository(IMongoDatabase mongoDatabase, string collectionName)
        {
            MongoCollection = mongoDatabase.GetCollection<TEntity>(collectionName);
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            entity.Audit.Modify();
            await MongoCollection.InsertOneAsync(entity);
            return entity;
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            await MongoCollection.DeleteOneAsync(deleteEntity => deleteEntity.Id == entity.Id);
        }

        public virtual async Task<IList<TEntity>> GetAllAsync()
        {
            return await MongoCollection.Find(entity => true).ToListAsync();
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            entity.Audit.Modify();
            await MongoCollection.FindOneAndReplaceAsync(updatedEntity => updatedEntity.Id == entity.Id, entity);
            return entity;
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await MongoCollection.Find(entity => entity.Id == id).SingleOrDefaultAsync();
        }
    }
}
