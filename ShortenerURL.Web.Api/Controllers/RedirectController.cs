﻿using Microsoft.AspNetCore.Mvc;

namespace ShortenerURL.Web.Api.Controllers
{
    public class RedirectController : Controller
    {
        [Route("{shortUrlId}")]
        public IActionResult ShortUrlRedirect(string shortUrlId)
        {
            return Redirect($"~/Redirect/{shortUrlId}");
        }

        [Route("/")]
        public IActionResult ManagePageRedirect()
        {
            return Redirect("~/manage/");
        }
    }
}