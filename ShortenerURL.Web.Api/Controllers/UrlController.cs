﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using ShortenerURL.Domain.UrlModels;
using Microsoft.AspNetCore.Mvc;
using ShortenerURL.Domain.Enums;
using ShortenerURL.Services.Interfaces;

namespace ShortenerURL.Web.Api.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class UrlController : BaseController
    {
        private readonly IUrlService _urlService;

        public UrlController(IUrlService urlService)
        {
            _urlService = urlService;
        }

        [HttpPost("create")]
        public async Task<UrlDataInfo> CreateUrlAsync([FromBody] NewUrl newUrlData)
        {
            return await _urlService.CreateAsync(CurrentAccountId, newUrlData);
        }

        [HttpDelete("delete")]
        public async Task DeleteUrlAsync(Guid urlId)
        {
            await _urlService.DeleteAsync(CurrentAccountId, urlId);
        }
        
        [HttpDelete("delete/all/expired")]
        public async Task DeleteAllExpiredUrlsAsync()
        {
            await _urlService.DeleteAllExpiredUrlsOnAccountAsync(CurrentAccountId);
        }
        
        [HttpDelete("delete/all/blocked-disposable")]
        public async Task DeleteAllBlockDisposableUrlsAsync()
        {
            await _urlService.DeleteAllBlockDisposableUrlsOnAccountAsync(CurrentAccountId);
        }
        
        [HttpDelete("delete/all")]
        public async Task DeleteAllUrlsAsync()
        {
            await _urlService.DeleteAllUrlsOnAccountAsync(CurrentAccountId);
        }

        [HttpGet("all")]
        public async Task<IList<UrlDataInfo>> GetAllAsync()
        {
            return await _urlService.GetAllAsync(CurrentAccountId);
        }

        [HttpGet("find/{urlId:guid}")]
        public async Task<UrlDataInfo> GetByIdAsync(Guid urlId)
        {
            return await _urlService.GetByIdAsync(CurrentAccountId, urlId);
        }

        [HttpPut("update")]
        public async Task<UrlDataInfo> UpdateAsync([FromBody] UpdateUrl updateUrlData)
        {
            return await _urlService.UpdateAsync(CurrentAccountId, updateUrlData);
        }

        [HttpGet("paginate")]
        public async Task<UrlPaginate> Paginate(int pageIndex, int pageSize, OrderMode orderMode, bool orderAsc)
        {
            return await _urlService.Paginate(pageIndex, pageSize, orderMode, orderAsc, CurrentAccountId);
        }
    }
}