﻿using System;
using Microsoft.AspNetCore.Mvc;
using ShortenerURL.Utils.Authorization;

namespace ShortenerURL.Web.Api.Controllers
{
    [ApiController]
    public class BaseController : Controller
    {
        protected Guid CurrentAccountId => AccountManager.GetCurrentAccountId(User);
    }
}