﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShortenerURL.Domain.AuthModels;
using ShortenerURL.Services.Interfaces;
using ShortenerURL.Utils.Authorization;

namespace ShortenerURL.Web.Api.Controllers
{
    [Route("[controller]")]
    public class AccountController : BaseController
    {
        private readonly IAccountProfileService _accountProfileService;

        public AccountController(IAccountProfileService accountProfileService)
        {
            _accountProfileService = accountProfileService;
        }

        [HttpPost("create")]
        public async Task CreateAccountAsync([FromBody] AccountProfileWithConfirmation newAccount)
        {
            await _accountProfileService.CreateAsync(newAccount);
        }

        [Authorize]
        [HttpDelete("delete")]
        public async Task DeleteAccountAsync(string password)
        {
            await _accountProfileService.DeleteAsync(password, CurrentAccountId);
            Response.Cookies.Delete("_au");
        }

        [HttpGet("all")]
        public async Task<IList<AccountProfileInfo>> GetAllAccountsAsync()
        {
            return await _accountProfileService.GetAllAsync();
        }

        [Authorize]
        [HttpPut("update")]
        public async Task UpdateAccountAsync([FromBody] AccountProfileUpdate updateAccountData)
        {
            await _accountProfileService.UpdateAsync(CurrentAccountId, updateAccountData);
        }
        
        [Authorize]
        [HttpGet("check")]
        public async Task CheckAuthAsync()
        {
            await Task.CompletedTask;
        }

        [Authorize]
        [HttpGet("profile")]
        public async Task<ProfileModel> GetProfile()
        {
            return await _accountProfileService.GetByIdAsync(CurrentAccountId);
        }

        [HttpPost("password-recovery")]
        public async Task RecoveryPassword([FromBody] PasswordRecoveryModel passwordRecoveryModel)
        {
            await _accountProfileService.PasswordRecovery(passwordRecoveryModel);
        }

        [HttpPost("login")]
        public async Task Login([FromBody] LoginModel loginModel)
        {
            var identity = await _accountProfileService.GetClaimsIdentityByCredentialsAsync(loginModel);

            var jwt = AuthOptions.GetJwtToken(identity);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            Response.Cookies.Append("_au", encodedJwt, new CookieOptions
            {
                Path = "/",
                Expires = DateTime.UtcNow.Add(TimeSpan.FromMinutes(AuthOptions.Lifetime)),
                HttpOnly = true,
                Secure = true
            });
        }

        [Authorize]
        [HttpPost("logout")]
        public void Logout()
        {
            Response.Cookies.Delete("_au");
        }
    }
}