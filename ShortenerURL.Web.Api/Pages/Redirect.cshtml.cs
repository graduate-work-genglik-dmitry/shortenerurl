﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShortenerURL.Domain.UrlModels;
using ShortenerURL.Services.Interfaces;
using ShortenerURL.Utils;
using ShortenerURL.Utils.Cryptography;
using UAParser;

namespace ShortenerURL.Web.Api.Pages
{
    [IgnoreAntiforgeryToken]
    public class RedirectModel : PageModel
    {
        private readonly IUrlService _urlService;
        private UrlDataInfo _urlData;

        public RedirectModel(IUrlService urlService)
        {
            _urlService = urlService;
        }

        public string Localization => GetLocalization();
        public string Id { get; private set; }
        public string SecretQuestionText { get; private set; }
        public bool IsCorrect { get; private set; } = true;
        public bool IsExpired { get; private set; }
        public bool? IsPassword { get; private set; }
        public bool? IsSecretQuestion { get; private set; }
        public bool IsInvalidPassword { get; private set; }
        public bool IsInvalidSecretQuestion { get; private set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            Id = id;
            _urlData = await _urlService.GetByShortUrlIdAsync(id);

            LocalizationCheck(Response);

            if (_urlData == null || id == null || string.IsNullOrEmpty(id))
            {
                IsCorrect = false;
                return null;
            }

            if (_urlData.Expiration != null && _urlData.Expiration < DateTime.UtcNow)
            {
                IsExpired = true;
                return null;
            }

            if (_urlData.IsDisposable == true && _urlData.Clicks > 0)
            {
                IsExpired = true;
                return null;
            }

            IsPassword = _urlData.Password != null;
            IsSecretQuestion = _urlData.SecretQuestion != null;
            SecretQuestionText = _urlData.SecretQuestionText;

            if (IsPassword == false && IsSecretQuestion == false)
            {
                await UpdateStatistics(Request);
                return Redirect(AesCryptUtil.Decrypt(_urlData.SourceUrl, _urlData.ShortUrlId));
            }

            return null;
        }

        public async Task<IActionResult> OnPostAsync(string passwordOrSecret, string id)
        {
            Id = id;
            _urlData = await _urlService.GetByShortUrlIdAsync(id);

            LocalizationCheck(Response);

            IsPassword = _urlData.Password != null;
            IsSecretQuestion = _urlData.SecretQuestion != null;
            SecretQuestionText = _urlData.SecretQuestionText;

            if (IsPassword == true)
            {
                if (HashUtil.CheckMatch(passwordOrSecret, _urlData.Password))
                {
                    await UpdateStatistics(Request);
                    return Redirect(AesCryptUtil.Decrypt(_urlData.SourceUrl, _urlData.ShortUrlId));
                }

                IsInvalidPassword = true;
            }

            if (IsSecretQuestion == true)
            {
                if (HashUtil.CheckMatch(passwordOrSecret, _urlData.SecretQuestion))
                {
                    await UpdateStatistics(Request);
                    return Redirect(AesCryptUtil.Decrypt(_urlData.SourceUrl, _urlData.ShortUrlId));
                }

                IsInvalidSecretQuestion = true;
            }

            return null;
        }

        private async Task UpdateStatistics(HttpRequest request)
        {
            var localhostDemoCrutch = new List<string>
            {
                "118.82.130.61", "81.18.140.93", "195.239.0.254", "85.26.164.0",
                "198.109. 124.3", "109.228.67.209", "5.9.109.158", "204.9.109.158", "47.9.109.158", "103.9.109.93",
                "187.92.109.93", "157.92.123.93", "210.141.244.33", "212.141.244.33", "212.149.244.33", "220.149.123.33",
                "2.11.123.33", "3.11.123.33", "12.23.123.33", "189.23.123.33", "173.34.123.33", "133.34.123.33"
            };

            var remote = localhostDemoCrutch[new Random().Next(localhostDemoCrutch.Count - 1)];//HttpContext.Connection.RemoteIpAddress; 

            var location = await LocationUtil.GetLocation(remote?.ToString());

            var userAgentString = request.Headers["User-Agent"].ToString();
            var uaParser = Parser.GetDefault();
            var info = uaParser.Parse(userAgentString);

            var stats = new UrlStatisticsInfo
            {
                Ip = remote?.ToString(),
                TimeOfVisit = DateTime.UtcNow,
                Location = location.Status == "success"
                    ? $"{location.City}, {location.RegionName}, {location.Country}"
                    : "Unknown",
                Device = (string.IsNullOrWhiteSpace(info.Device.Family) || info.Device.Family == "Other"
                    ? "Desktop Device"
                    : info.Device.Family) + $" with {info.UA.Family}",
                OperationSystem = $"{info.OS.Family} {info.OS.Major}"
            };

            await _urlService.AddStatisticsAsync(Id, stats);
        }

        private void LocalizationCheck(HttpResponse response)
        {
            if (!HttpContext.Request.Cookies.ContainsKey("PageLang"))
            {
                response.Cookies.Append("PageLang", "En", new CookieOptions
                {
                    Path = "/Redirect"
                });
            }
        }

        private string GetLocalization()
        {
            return HttpContext.Request.Cookies.ContainsKey("PageLang") ? HttpContext.Request.Cookies["PageLang"] : "En";
        }
    }
}