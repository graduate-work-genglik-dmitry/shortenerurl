﻿using System;
using System.Threading.Tasks;
using FluentEmail.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ShortenerURL.Data;
using ShortenerURL.Utils.Cryptography;
using ShortenerURL.Utils.Email;

namespace ShortenerURL.Web.Api.Pages
{
    [IgnoreAntiforgeryToken]
    public class ConfirmationModel : PageModel
    {
        private readonly IAccountProfileRepository _accountProfileRepository;
        private readonly IFluentEmail _emailService;

        public bool AccountDoesNotExist { get; private set; }
        public bool AccountAlreadyConfirm { get; private set; }
        public string Email { get; private set; }

        public ConfirmationModel(IAccountProfileRepository accountProfileRepository, IFluentEmail emailService)
        {
            _accountProfileRepository = accountProfileRepository;
            _emailService = emailService;
        }

        public async Task OnGetAsync(string encryptAccountId)
        {
            string id;
            encryptAccountId = encryptAccountId.Replace("-$", "/");

            try
            {
                id = AesCryptUtil.Decrypt(encryptAccountId);
            }
            catch (FormatException)
            {
                AccountDoesNotExist = true;
                return;
            }

            var account = await _accountProfileRepository.GetByIdAsync(Guid.ParseExact(id, "N"));

            if (account == null)
            {
                AccountDoesNotExist = true;
                return;
            }

            Email = AesCryptUtil.Decrypt(account.Email);

            switch (account.Confirmed)
            {
                case true:
                    AccountAlreadyConfirm = true;
                    return;
                case false:
                {
                    account.Confirmed = true;

                    if (!account.AccessIsAllowed)
                    {
                        account.AccessIsAllowed = true;
                    }

                    break;
                }
            }

            await _accountProfileRepository.UpdateAsync(account);

            var emailParameters = new EmailParameters
            {
                Subject = "Email confirmed in SAPL",
                Recipient = AesCryptUtil.Decrypt(account.Email),
                TemplateName = EmailTemplate.SuccessfulConfirmation,
                Model = new
                {
                    Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                }
            };

            await EmailSender.SendEmail(_emailService, emailParameters);
        }
    }
}