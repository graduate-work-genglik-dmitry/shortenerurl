﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ShortenerURL.Web.Api.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [IgnoreAntiforgeryToken]
    public class ErrorModel : PageModel
    {
        public string ExceptionMessage { get; private set; }

        public void OnGet() => ErrorHandler();
        public void OnPost() => ErrorHandler();
        public void OnDelete() => ErrorHandler();
        public void OnPut() => ErrorHandler();

        private void ErrorHandler()
        {
            var exceptionHandlerPathFeature =
                HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionHandlerPathFeature?.Error.Message is { } msg)
            {
                ExceptionMessage = $"An error has occurred: {msg}";
            }
            else
            {
                var code = HttpContext.Request.Query["code"];
                ExceptionMessage = code == "401"
                    ? "Action prohibited. You are not logged in."
                    : $"Something went wrong! Error code: {code}";
            }
        }
    }
}