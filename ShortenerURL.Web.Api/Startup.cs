using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using ShortenerURL.Data;
using ShortenerURL.Data.MongoDB;
using ShortenerURL.Data.MongoDB.Repositories;
using ShortenerURL.Services.Interfaces;
using ShortenerURL.Services.Services;
using ShortenerURL.Utils.Authorization;
using ShortenerURL.Utils.Email;

namespace ShortenerURL.Web.Api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder =
                new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Configuration = builder.Build();
            DataLayerConfiguration.Configure();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 4444;
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(AuthOptions.JwtBearerOptions);

            services.AddRazorPages();
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors();

            services.AddFluentEmail(EmailSender.Email).AddRazorRenderer()
                .AddMailKitSender(EmailSender.ClientOptions);

            services.AddSingleton(typeof(IMongoDatabase), serviceProvider =>
            {
                var mongoDbConnectionString = Configuration.GetConnectionString("MongoDbOnline");
                return DataLayerConfiguration.GetDatabase(mongoDbConnectionString);
            });
            services.AddTransient<IUrlRepository, UrlRepository>();
            services.AddTransient<IAccountProfileRepository, AccountProfileRepository>();
            services.AddTransient<IUrlService, UrlService>();
            services.AddTransient<IAccountProfileService, AccountProfileService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UsePathBase("/api");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseStatusCodePagesWithReExecute("/Error", "?code={0}");
            app.UseHttpsRedirection();

            app.UseCors(config =>
            {
                config.AllowAnyOrigin();
                config.AllowAnyMethod();
                config.AllowAnyHeader();
            });
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}