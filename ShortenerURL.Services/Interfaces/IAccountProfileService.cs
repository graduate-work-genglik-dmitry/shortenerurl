﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ShortenerURL.Domain.AuthModels;

namespace ShortenerURL.Services.Interfaces
{
    public interface IAccountProfileService
    {
        Task<IList<AccountProfileInfo>> GetAllAsync();
        Task<AccountProfileInfo> CreateAsync(AccountProfileWithConfirmation profile);
        Task DeleteAsync(string password, Guid profileId);
        Task<AccountProfileInfo> UpdateAsync(Guid accountId, AccountProfileUpdate newProfileData);
        Task<ProfileModel> GetByIdAsync(Guid profileId);
        Task<ClaimsIdentity> GetClaimsIdentityByCredentialsAsync(LoginModel loginWithConfirmation);
        Task PasswordRecovery(PasswordRecoveryModel passwordRecoveryModel);
    }
}