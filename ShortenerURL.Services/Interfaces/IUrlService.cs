﻿using ShortenerURL.Domain.UrlModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShortenerURL.Domain.Enums;

namespace ShortenerURL.Services.Interfaces
{
    public interface IUrlService
    {
        Task<IList<UrlDataInfo>> GetAllAsync(Guid accountId);
        Task<UrlDataInfo> CreateAsync(Guid accountId, NewUrl newUrl);
        Task DeleteAsync(Guid accountId, Guid urlId);
        Task DeleteAllExpiredUrlsOnAccountAsync(Guid accountId);
        Task DeleteAllBlockDisposableUrlsOnAccountAsync(Guid accountId);
        Task<UrlDataInfo> UpdateAsync(Guid accountId, UpdateUrl urlUpdateData);
        Task AddStatisticsAsync(string shortUrlId, UrlStatisticsInfo statisticsInfo);
        Task<UrlDataInfo> GetByIdAsync(Guid accountId, Guid urlId);
        Task<UrlDataInfo> GetByShortUrlIdAsync(string shortUrlId);
        Task DeleteAllUrlsOnAccountAsync(Guid accountId);
        Task<UrlPaginate> Paginate(int pageIndex, int pageSize, OrderMode orderMode, bool orderAsc, Guid accountId);
    }
}