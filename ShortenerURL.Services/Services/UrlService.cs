﻿using ShortenerURL.Data;
using ShortenerURL.Domain.Enums;
using ShortenerURL.Domain.UrlModels;
using ShortenerURL.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using ShortenerURL.Exceptions;
using ShortenerURL.Utils;
using ShortenerURL.Utils.Cryptography;

namespace ShortenerURL.Services.Services
{
    public class UrlService : IUrlService
    {
        private readonly IUrlRepository _urlRepository;

        public UrlService(IUrlRepository urlRepository)
        {
            _urlRepository = urlRepository;
        }

        public async Task<UrlDataInfo> CreateAsync(Guid accountId, NewUrl newUrlData)
        {
            ValidateUtil.UrlValidator(newUrlData);

            var newUrl = new UrlDataInfo
            {
                AccountId = accountId,
                ShortUrlId = ShortUrlGenerateUtil.GenerateShortUrlId,
                Lifetime = newUrlData.Lifetime,
                IsEternal = newUrlData.IsEternal,
                Mode = newUrlData.Mode
            };

            //Needed to check the uniqueness of the generated link
            var forCheckUniqueness =
                ShortUrlGenerateUtil.Uniqueness(await GetByShortUrlIdAsync(newUrl.ShortUrlId), newUrl.ShortUrlId);
            while (forCheckUniqueness != string.Empty)
            {
                newUrl.ShortUrlId = forCheckUniqueness;
                forCheckUniqueness =
                    ShortUrlGenerateUtil.Uniqueness(await GetByShortUrlIdAsync(newUrl.ShortUrlId), newUrl.ShortUrlId);
            }

            newUrl.SourceUrl =
                AesCryptUtil.Encrypt(PunycodeConvertUtil.Encode(newUrlData.SourceUrl), newUrl.ShortUrlId);

            switch (newUrlData.Mode)
            {
                case AccessMode.Disposable:
                    newUrl.IsDisposable = true;
                    newUrl.Expiration = null;
                    break;
                default:
                {
                    newUrl.IsDisposable = false;
                    switch (newUrlData.IsEternal)
                    {
                        case false:
                        {
                            if (newUrlData.Lifetime != null)
                                newUrl.Expiration = DateTime.UtcNow.AddMinutes((double) newUrlData.Lifetime).AddSeconds(0 - DateTime.UtcNow.Second);
                            break;
                        }
                        default:
                            newUrl.Expiration = null;
                            break;
                    }

                    break;
                }
            }

            if (newUrlData.Mode != AccessMode.Free)
            {
                switch (newUrlData.Mode)
                {
                    case AccessMode.SecretQuestion:
                        newUrl.SecretQuestion = HashUtil.GetHashString(newUrlData.PasswordOrSecret);
                        newUrl.SecretQuestionText = newUrlData.SecretQuestionText;
                        newUrl.Password = null;
                        break;
                    case AccessMode.Password:
                        newUrl.Password = HashUtil.GetHashString(newUrlData.PasswordOrSecret);
                        newUrl.SecretQuestion = null;
                        newUrl.SecretQuestionText = null;
                        break;
                }
            }
            else
            {
                newUrlData.PasswordOrSecret = null;
            }

            if (newUrl.IsEternal == true)
            {
                newUrl.Lifetime = 0;
            }

            return await _urlRepository.CreateAsync(newUrl);
        }

        public async Task DeleteAsync(Guid accountId, Guid id)
        {
            var url = await _urlRepository.GetByIdAsync(accountId, id);
            if (url == null)
            {
                throw new UrlNotExistsException();
            }

            if (accountId != url.AccountId)
            {
                throw new NoAccessException("You do not have delete access");
            }

            await _urlRepository.DeleteAsync(url);
        }

        public async Task DeleteAllExpiredUrlsOnAccountAsync(Guid accountId)
        {
            await _urlRepository.DeleteAllExpiredUrlsOnAccountAsync(accountId);
        }

        public async Task DeleteAllBlockDisposableUrlsOnAccountAsync(Guid accountId)
        {
            await _urlRepository.DeleteAllBlockDisposableUrlsOnAccountAsync(accountId);
        }

        public async Task DeleteAllUrlsOnAccountAsync(Guid accountId)
        {
            await _urlRepository.DeleteAllUrlsOnAccountAsync(accountId);
        }

        public async Task<IList<UrlDataInfo>> GetAllAsync(Guid accountId)
        {
            var urls = await _urlRepository.GetAllAsync(accountId);

            foreach (var url in urls)
            {
                url.SourceUrl = AesCryptUtil.Decrypt(url.SourceUrl, url.ShortUrlId);
            }

            return urls;
        }

        public async Task<UrlDataInfo> UpdateAsync(Guid accountId, UpdateUrl urlUpdateData)
        {
            var url = await _urlRepository.GetByIdAsync(accountId, urlUpdateData.UrlId);
            if (url == null)
            {
                throw new UrlNotExistsException();
            }

            if (accountId != url.AccountId)
            {
                throw new NoAccessException("You do not have write access");
            }

            ValidateUtil.UpdateUrlDataValidator(urlUpdateData, url);

            if (urlUpdateData.IsEternal != null)
            {
                url.IsEternal = urlUpdateData.IsEternal;
                url.Expiration = null;
            }

            if (urlUpdateData.IsEternal is true)
            {
                url.Lifetime = 0;
            }

            if (urlUpdateData.Password != null)
            {
                if (!ValidateUtil.PasswordValidator(urlUpdateData.Password))
                {
                    throw new InvalidPasswordException();
                }

                url.Password = HashUtil.GetHashString(urlUpdateData.Password);
            }

            if (urlUpdateData.SecretQuestion != null)
            {
                url.SecretQuestion = HashUtil.GetHashString(urlUpdateData.SecretQuestion);
                url.SecretQuestionText = urlUpdateData.SecretQuestionText;
            }

            if (urlUpdateData.IsEternal is null or false && url.IsEternal == false)
            {
                if (urlUpdateData.AddedLifetime != null)
                {
                    var newLifetime = urlUpdateData.AddedLifetime;
                    if (url.Lifetime != null && url.Lifetime != 0)
                    {
                        newLifetime += url.Lifetime;
                        var newExpirationData = url.Audit.CreatedOn.AddMinutes((double) newLifetime).AddSeconds(0 - DateTime.UtcNow.Second);

                        if (newExpirationData > DateTime.UtcNow.AddDays(31))
                        {
                            throw new UpdateLifetimeException("The link cannot be extended for more than a month");
                        }

                        url.Expiration = newExpirationData;
                    }
                    else
                    {
                        url.Expiration = DateTime.UtcNow.AddMinutes((double) newLifetime);
                    }

                    url.Lifetime = newLifetime;
                }

                if (urlUpdateData.ReducedLifetime != null)
                {
                    if (url.Lifetime == null)
                    {
                        throw new UpdateLifetimeException("Zero lifetime cannot be reduced");
                    }

                    if (url.Lifetime == 0)
                    {
                        url.Expiration = DateTime.UtcNow.AddMinutes(0 - (double)urlUpdateData.ReducedLifetime).AddSeconds(0 - DateTime.UtcNow.Second);
                        var res = (TimeSpan) (url.Expiration - url.Audit.CreatedOn.AddSeconds(0 - DateTime.UtcNow.Second));
                        url.Lifetime = (long)res.TotalMinutes;
                    }
                    else
                    {
                        url.Lifetime -= urlUpdateData.ReducedLifetime;
                    }
                    if (url.Lifetime < 15)
                    {
                        throw new UpdateLifetimeException("Life time less than 15 minutes");
                    }

                    url.Expiration = url.Audit.CreatedOn.AddMinutes((double) url.Lifetime);
                }
            }

            return await _urlRepository.UpdateAsync(url);
        }

        public async Task AddStatisticsAsync(string shortUrlId, UrlStatisticsInfo statisticsInfo)
        {
            await _urlRepository.AddStatisticsAsync(shortUrlId, statisticsInfo);
        }

        public async Task<UrlDataInfo> GetByIdAsync(Guid accountId, Guid id)
        {
            var url = await _urlRepository.GetByIdAsync(accountId, id);
            if (url == null)
            {
                throw new UrlNotExistsException();
            }

            if (accountId != url.AccountId)
            {
                throw new NoAccessException("You do not have access to search");
            }

            url.SourceUrl = AesCryptUtil.Decrypt(url.SourceUrl, url.ShortUrlId);

            return url;
        }

        public async Task<UrlDataInfo> GetByShortUrlIdAsync(string shortUrlId)
        {
            return await _urlRepository.GetByShortUrlIdAsync(shortUrlId);
        }

        public async Task<UrlPaginate> Paginate(int pageIndex, int pageSize, OrderMode orderMode, bool orderAsc,
            Guid accountId)
        {
            var sortDefinition = orderMode switch
            {
                OrderMode.ModifiedOn => orderAsc
                    ? Builders<UrlDataInfo>.Sort.Ascending(url => url.Audit.ModifiedOn)
                    : Builders<UrlDataInfo>.Sort.Descending(url => url.Audit.ModifiedOn),
                OrderMode.CreatedOn => orderAsc
                    ? Builders<UrlDataInfo>.Sort.Ascending(url => url.Audit.CreatedOn)
                    : Builders<UrlDataInfo>.Sort.Descending(url => url.Audit.CreatedOn),
                OrderMode.RedirectCount => orderAsc
                    ? Builders<UrlDataInfo>.Sort.Ascending(url => url.Clicks)
                    : Builders<UrlDataInfo>.Sort.Descending(url => url.Clicks),
                OrderMode.Expiration => orderAsc
                    ? Builders<UrlDataInfo>.Sort.Ascending(url => url.Expiration)
                    : Builders<UrlDataInfo>.Sort.Descending(url => url.Expiration),
                _ => throw new ArgumentOutOfRangeException(nameof(orderMode), orderMode, null)
            };

            var startIndex = pageIndex * pageSize;
            var sortedUrls = await _urlRepository.GetRangeAsync(accountId, sortDefinition, startIndex, pageSize);

            foreach (var url in sortedUrls)
            {
                url.SourceUrl = AesCryptUtil.Decrypt(url.SourceUrl, url.ShortUrlId);
            }

            var paginateResult = new UrlPaginate
            {
                IsEmpty = sortedUrls.Count == 0,
                TotalLength = await _urlRepository.GetUrlsCountOnAccount(accountId),
                PageIndex = pageIndex,
                GlobalStartIndex = startIndex,
                PageItems = sortedUrls
            };

            return paginateResult;
        }
    }
}