﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentEmail.Core;
using ShortenerURL.Data;
using ShortenerURL.Domain.AuthModels;
using ShortenerURL.Exceptions;
using ShortenerURL.Services.Interfaces;
using ShortenerURL.Utils;
using ShortenerURL.Utils.Authorization;
using ShortenerURL.Utils.Cryptography;
using ShortenerURL.Utils.Email;

namespace ShortenerURL.Services.Services
{
    public class AccountProfileService : IAccountProfileService
    {
        private readonly IAccountProfileRepository _accountProfileRepository;
        private readonly IUrlService _urlService;
        private readonly IFluentEmail _emailService;

        public AccountProfileService(
            IAccountProfileRepository accountProfileRepository,
            IUrlService urlService,
            IFluentEmail emailService)
        {
            _accountProfileRepository = accountProfileRepository;
            _urlService = urlService;
            _emailService = emailService;
        }

        public async Task<IList<AccountProfileInfo>> GetAllAsync()
        {
            return await _accountProfileRepository.GetAllAsync();
        }

        public async Task<AccountProfileInfo> CreateAsync(AccountProfileWithConfirmation profile)
        {
            profile.Email = profile.Email.ToLower();
            
            if (profile.Email == null)
            {
                throw new AccountCredentialsException("No email specified");
            }

            ValidateUtil.AccountProfileValidator(profile.Password, profile.RepeatPassword,
                profile.Username, profile.Email);

            var passwordForAes = HashUtil.GetHashString(profile.Password + "AES256", false);

            var accountByUsername =
                await _accountProfileRepository.GetByUsernameAsync(AesCryptUtil.Encrypt(profile.Username, passwordForAes));
            var accountByEmail = await _accountProfileRepository.GetByEmailAsync(AesCryptUtil.Encrypt(profile.Email));

            if (accountByUsername != null || accountByEmail != null)
            {
                throw new AccountAlreadyExistsException();
            }

            var newAccount = new AccountProfileInfo
            {
                Username = AesCryptUtil.Encrypt(profile.Username, passwordForAes),
                Email = AesCryptUtil.Encrypt(profile.Email),
                Password = HashUtil.GetHashString(profile.Password),
                Password2 = passwordForAes
            };

            var account = await _accountProfileRepository.CreateAsync(newAccount);

            var emailParameters = new EmailParameters
            {
                Subject = $"{AesCryptUtil.Decrypt(account.Username, account.Password2)}, please verify your account",
                Recipient = AesCryptUtil.Decrypt(account.Email),
                TemplateName = EmailTemplate.EmailConfirmation,
                Model = new
                {
                    Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                    CryptId = AesCryptUtil.Encrypt(account.Id.ToString("N")).Replace("/", "-$")
                }
            };

            await EmailSender.SendEmail(_emailService, emailParameters);

            return account;
        }

        public async Task DeleteAsync(string password, Guid profileId)
        {
            var account = await _accountProfileRepository.GetByIdAsync(profileId);

            if (account == null)
            {
                throw new AccountNotExistsException();
            }

            if (!HashUtil.CheckMatch(password, account.Password))
            {
                throw new NoAccessException("Action prohibited");
            }

            await _urlService.DeleteAllUrlsOnAccountAsync(account.Id);
            await _accountProfileRepository.DeleteAsync(account);

            if (account.Confirmed)
            {
                var emailParameters = new EmailParameters
                {
                    Subject = "Account has been deleted",
                    Recipient = AesCryptUtil.Decrypt(account.Email),
                    TemplateName = EmailTemplate.AccountHasBeenDelete,
                    Model = new
                    {
                        Name = AesCryptUtil.Decrypt(account.Username, account.Password2)
                    }
                };

                await EmailSender.SendEmail(_emailService, emailParameters);
            }
        }

        public async Task<AccountProfileInfo> UpdateAsync(Guid accountId, AccountProfileUpdate newProfileData)
        {
            var account = await _accountProfileRepository.GetByIdAsync(accountId);

            if (account == null)
            {
                throw new AccountNotExistsException();
            }

            ValidateUtil.UpdateAccountProfileValidator(account, newProfileData);

            if (newProfileData.Username != null)
            {
                var accountWithSameUsername =
                    await _accountProfileRepository.GetByUsernameAsync(AesCryptUtil.Encrypt(newProfileData.Username,
                        account.Password2));

                if (accountWithSameUsername != null && accountWithSameUsername.Id != accountId)
                {
                    throw new AccountAlreadyExistsException();
                }
            }

            if (newProfileData.Email != null)
            {
                newProfileData.Email = newProfileData.Email.ToLower();
                var accountWithSameEmail =
                    await _accountProfileRepository.GetByEmailAsync(AesCryptUtil.Encrypt(newProfileData.Email));

                if (accountWithSameEmail != null && accountWithSameEmail.Id != accountId)
                {
                    throw new AccountAlreadyExistsException();
                }
            }

            var oldEmail = string.Empty;
            if (newProfileData.Email != null)
            {
                oldEmail = AesCryptUtil.Decrypt(account.Email);
            }

            account.Password = HashUtil.GetHashString(newProfileData.NewPassword) ?? account.Password;
            
            if (newProfileData.NewPassword != null)
            {
                var username = AesCryptUtil.Decrypt(account.Username, account.Password2);
                account.Password2 = HashUtil.GetHashString(newProfileData.NewPassword + "AES256", false);
                account.Username = AesCryptUtil.Encrypt(username, account.Password2);
            }
            else
            {
                account.Username = AesCryptUtil.Encrypt(newProfileData.Username, account.Password2) ?? account.Username;
            }
            account.Email = AesCryptUtil.Encrypt(newProfileData.Email) ?? account.Email;

            if (newProfileData.NewPassword != null && account.Confirmed)
            {
                var emailParameters = new EmailParameters
                {
                    Subject = "Password has been changed",
                    Recipient = AesCryptUtil.Decrypt(account.Email),
                    TemplateName = EmailTemplate.PasswordHasBeenChanged,
                    Model = new
                    {
                        Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                    }
                };

                await EmailSender.SendEmail(_emailService, emailParameters);
            }

            if (newProfileData.Email != null && account.AccessIsAllowed)
            {
                account.Confirmed = false;

                var emailParameters = new EmailParameters
                {
                    Subject = "Your email has been updated",
                    Recipient = oldEmail,
                    TemplateName = EmailTemplate.EmailHasBeenUpdated,
                    Model = new
                    {
                        Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                        NewEmail = newProfileData.Email
                    }
                };

                await EmailSender.SendEmail(_emailService, emailParameters);

                var emailParametersForNewAddress = new EmailParameters
                {
                    Subject =
                        $"{AesCryptUtil.Decrypt(account.Username, account.Password2)}, please verify your new Email",
                    Recipient = newProfileData.Email,
                    TemplateName = EmailTemplate.EmailConfirmationAfterChange,
                    Model = new
                    {
                        Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                        CryptId = AesCryptUtil.Encrypt(account.Id.ToString("N")).Replace("/", "-$")
                    }
                };

                await EmailSender.SendEmail(_emailService, emailParametersForNewAddress);
            }

            return await _accountProfileRepository.UpdateAsync(account);
        }

        public async Task<ProfileModel> GetByIdAsync(Guid profileId)
        {
            var account = await _accountProfileRepository.GetByIdAsync(profileId);

            if (account == null)
            {
                throw new AccountNotExistsException();
            }

            var profile = new ProfileModel
            {
                Id = account.Id,
                Username = AesCryptUtil.Decrypt(account.Username, account.Password2),
                Email = AesCryptUtil.Decrypt(account.Email),
                Confirmed = account.Confirmed,
                AccessIsAllowed = account.AccessIsAllowed
            };

            return profile;
        }

        public async Task<ClaimsIdentity> GetClaimsIdentityByCredentialsAsync(LoginModel loginModel)
        {
            AccountProfileInfo account;

            if (loginModel.Email)
            {
                ValidateUtil.AccountProfileValidator(loginModel.Password, email: loginModel.UsernameOrEmail.ToLower());
                var emailEncrypt = AesCryptUtil.Encrypt(loginModel.UsernameOrEmail.ToLower());
                account = await _accountProfileRepository.GetByEmailAsync(emailEncrypt);
            }
            else
            {
                ValidateUtil.AccountProfileValidator(loginModel.Password, username: loginModel.UsernameOrEmail);
                var passwordForAes = HashUtil.GetHashString(loginModel.Password + "AES256", false);
                var usernameEncrypt = AesCryptUtil.Encrypt(loginModel.UsernameOrEmail, passwordForAes);
                account = await _accountProfileRepository.GetByUsernameAsync(usernameEncrypt);
            }

            if (account == null)
            {
                throw new AccountNotExistsException();
            }

            if (!HashUtil.CheckMatch(loginModel.Password, account.Password))
            {
                throw new NoAccessException("Wrong login or password ");
            }

            var claims = new List<Claim>
            {
                new(ClaimsIdentity.DefaultNameClaimType, loginModel.UsernameOrEmail),
                new(CustomClaims.Id, account.Id.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            return claimsIdentity;
        }

        public async Task PasswordRecovery(PasswordRecoveryModel passwordRecoveryModel)
        {
            var account = await _accountProfileRepository.
                GetByEmailAsync(AesCryptUtil.Encrypt(passwordRecoveryModel.Email.ToLower()));

            if (account == null)
            {
                throw new AccountNotExistsException();
            }

            if (!account.Confirmed)
            {
                throw new RecoveryPasswordException();
            }

            var newPassword = AccountManager.GenerateNewPassword();
            account.Password = HashUtil.GetHashString(newPassword);
            
            var username = AesCryptUtil.Decrypt(account.Username, account.Password2);
            account.Password2 = HashUtil.GetHashString(newPassword + "AES256", false);
            account.Username = AesCryptUtil.Encrypt(username, account.Password2);

            await _accountProfileRepository.UpdateAsync(account);

            var emailParameters = new EmailParameters
            {
                Subject = "Password Recovery",
                Recipient = AesCryptUtil.Decrypt(account.Email),
                TemplateName = EmailTemplate.PasswordRecovery,
                Model = new
                {
                    Name = AesCryptUtil.Decrypt(account.Username, account.Password2),
                    Password = newPassword
                }
            };

            await EmailSender.SendEmail(_emailService, emailParameters);
        }
    }
}